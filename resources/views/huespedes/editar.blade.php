@extends('layouts.project')

@section('htmlheader_title', "{$selectedHotel->nombre} | Editar huésped {$huesped->nombre_completo}")

@section('content_header_title')
    {{ $selectedHotel->nombre }}
@endsection

@section('content_header_description')
    Editar huésped <b>{{ $huesped->nombre_completo }}</b>
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form action="{{ route('huespedes.update', [$huesped]) }}" method="post" id="form_editar" name="form_editar">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="nombre">Nombres</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre(s)" value="{{ old('nombre', $huesped->nombre) }}">
                </div>
                <div class="form-group">
                    <label for="apellido">Apellidos</label>
                    <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido(s)" value="{{ old('apellido', $huesped->apellido) }}">
                </div>
                <div class="form-group">
                    <label for="obs">Observaciones</label>
                    <textarea class="form-control" rows="3" id="obs" name="obs" placeholder="Observaciones">{{ old('obs', $huesped->obs) }}</textarea>
                </div>
                <button type="submit" class="btn btn-success btn-block"><i class="fa fa-save"></i> Guardar</button>
                <a class="btn btn-info btn-xs btn-block" href="{{ route('huespedes.show', [$huesped]) }}" role="button">Retroceder</a>
            </form>
        </div>
    </div>
@endsection
@extends('layouts.project')

@section('htmlheader_title', "{$huesped->hotel->nombre} | Detalles del huesped {$huesped->nombre_completo}")

@section('content_header_title')
    {{ $huesped->hotel->nombre }}
@endsection

@section('content_header_description')
    Detalles del huésped {{ $huesped->nombre_completo }}
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Datos básicos</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="nombre">Nombres</label>
                        <input disabled type="text" class="form-control" id="nombre" name="nombre" value="{{ $huesped->nombre }}">
                    </div>
                    <div class="form-group">
                        <label for="apellido">Apellidos</label>
                        <input disabled type="text" class="form-control" id="apellido" name="apellido" value="{{ $huesped->apellido }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input disabled type="text" class="form-control" id="email" name="email" value="{{ $huesped->email }}">
                    </div>
                    @if( $huesped->obs )
                        <div class="form-group">
                            <label for="obs">Observaciones</label>
                            <textarea disabled class="form-control" rows="3" id="obs" name="obs" placeholder="Observaciones">{{ $huesped->obs }}</textarea>
                        </div>
                    @endif
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a class="btn btn-warning btn-block" href="{{ route('huespedes.edit', [$huesped]) }}" role="button"><i class="fa fa-edit"></i> Editar</a>
                    <a class="btn btn-info btn-xs btn-block" href="{{ route('huespedes.index') }}" role="button">Retroceder</a>
                </div>
                <!-- box-footer -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

{{--
@section('scripts')
    @parent
    <script>
        $(function() {

        });
    </script>
@endsection
--}}

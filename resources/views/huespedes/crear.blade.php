@extends('layouts.project')

@section('htmlheader_title', "{$selectedHotel->nombre} | Crear nuevo huésped")

@section('content_header_title')
    {{ $selectedHotel->nombre }}
@endsection

@section('content_header_description')
    Crear nuevo huésped
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form action="{{ route('huespedes.store') }}" method="post" id="form_crear" name="form_crear">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="nombre">Nombres</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre(s)" value="{{ old('nombre') }}">
                </div>
                <div class="form-group">
                    <label for="apellido">Apellidos</label>
                    <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido(s)" value="{{ old('apellido') }}">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
                </div>
                <div class="form-group">
                    <label for="obs">Observaciones</label>
                    <textarea class="form-control" rows="3" id="obs" name="obs" placeholder="Observaciones">{{ old('obs') }}</textarea>
                </div>

                <button type="submit" class="btn btn-success btn-lg btn-block">Crear</button>
                <a class="btn btn-info btn-xs btn-block" href="{{ route('huespedes.index') }}" role="button">Retroceder</a>
            </form>
        </div>
    </div>
@endsection
@extends('layouts.project')

@section('htmlheader_title', 'Huéspedes')

@section('content_header_title')
    Huéspedes
@endsection

@section('content_header_description')
    Listado de huéspedes
@endsection

@section('main-content')
    @include('hoteles.partials.selector_lista_hoteles')

    @if(session('selectedHotelId'))
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Huéspedes registrados en {{ $selectedHotel->nombre }}</h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-primary" href="{{ route('huespedes.create') }}" role="button" title="Crear registro de huésped"><i class="fa fa-plus-circle"></i> Crear registro de huésped</a>
                </div>
                <!-- /.box-tools -->
            </div>
            <div class="box-body">
                @if (count($huespedes) > 0)
                    <div class="table-responsive">
                        <table id="data_table" name="data_table" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th style="text-align: center;">Huésped</th>
                                <th style="text-align: center;">Email</th>
                                <th style="text-align: center;">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($huespedes as $huesped)
                                <tr style="text-align: center;">
                                    <td style="text-align: center; vertical-align: middle;">
                                        {{ $huesped->nombre_completo }}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        {{ $huesped->email }}
                                    </td>
                                    <td style="text-align: right; vertical-align: middle;">
                                        <div class="btn-group" role="group">
                                            {{-- Ver Registro --}}
                                            <a class="btn btn-xs btn-primary" href="{{ route('huespedes.show', [$huesped]) }}" role="button" title="Ver Detalles"><i class="fa fa-folder-open-o"></i> Ver Detalles</a>
                                            {{-- Editar registro --}}
                                            <a class="btn btn-xs btn-warning" href="{{ route('huespedes.edit', [$huesped]) }}" role="button" title="Editar registro"><i class="fa fa-edit"></i> Editar registro</a>
                                            {{-- Borrar registro --}}
                                            <form class="btn-group" action="{{ route('huespedes.destroy', [$huesped]) }}" method="post" onsubmit="return confirm('Confirma la eliminación de este registro?');">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-xs btn-danger" title="Borrar registro"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Borrar registro</button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-warning" role="alert" style="margin-top: 0; margin-bottom: 0;"><b>Aún no se han creado huespedes en el hotel seleccionado</b></div>
                @endif
            </div><!-- /.box-body -->
            <div class="box-footer">
                <a class="btn btn-primary pull-right" href="{{ route('huespedes.create') }}" role="button" title="Crear registro de huésped"><i class="fa fa-plus-circle"></i> Crear registro de huésped</a>
            </div><!-- box-footer -->
        </div><!-- /.box -->
    @else
        <div class="callout callout-info">
            <h4>Aviso</h4>
            <p>Debe seleccionar un hotel para poder ver el listado de huéspedes del mismo</p>
        </div>
    @endif
@endsection
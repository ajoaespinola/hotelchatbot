@extends("layouts.partials.sidebars.sidebar")

@section('sidebar_menu')
    <li class="{{ Request::is('home') ? "active" : "" }}"><a href="{{ route('home') }}"><i class="fa fa-home"></i> <span>Inicio</span></a></li>

    <li class="header">HOTELES</li>
    <li class="{{ Request::routeIs('habitaciones.index') ? "active" : "" }}">
        <a href="{{ route('habitaciones.index', [], false) }}">
            <i class="fa fa-circle-o"></i> <span>Habitaciones</span>
        </a>
    </li>
    <li class="{{ Request::routeIs('huespedes.index') ? "active" : "" }}">
        <a href="{{ route('huespedes.index', [], false) }}">
            <i class="fa fa-circle-o"></i> <span>Huéspedes</span>
        </a>
    </li>
    <li class="{{ Request::routeIs('reservas.index') ? "active" : "" }}">
        <a href="{{ route('reservas.index', [], false) }}">
            <i class="fa fa-circle-o"></i> <span>Reservas</span>
        </a>
    </li>

    <li class="header">SOPORTE A CLIENTE</li>
    <li class="{{ Request::routeIs('soporte.index') ? "active" : "" }}">
        <a href="{{ route('soporte.index', [], false) }}">
            <i class="fa fa-circle-o"></i> <span>Visor de conversaciones</span>
        </a>
    </li>

    <li class="header">ADMINISTRACIÓN</li>
    <li class="{{ Request::routeIs('hoteles.index') ? "active" : "" }}">
        <a href="{{ route('hoteles.index', [], false) }}">
            <i class="fa fa-circle-o"></i> <span>Hoteles</span>
        </a>
    </li>
@endsection
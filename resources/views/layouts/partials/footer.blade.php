<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    @auth
        <div class="pull-right hidden-xs">
            <a href="#" data-toggle="modal" data-target="#errorReport"><b>Reportar un problema</b></a>
        </div>
    @endauth
    <!-- Default to the left -->
    <strong>Copyright &copy; 2020 - {{ \Carbon\Carbon::now()->year }}</strong> <a href="https://espino.la/" target="_blank">espino.la</a>.
</footer>

@auth
    @include('layouts.partials.modals.modal', [
        'modalId' => 'errorReport',
        'modalTitle' => trans('app.actions.report_issue'),
        'size' => "modal-lg", // modal-lg, modal-sm or empty to default
    ])
@endauth

{{-- the js code who fill the modal content is on project.blade.php because it needs jquery loaded--}}
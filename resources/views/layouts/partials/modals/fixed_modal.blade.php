<div class="modal fade" id="{{ $modalId }}" tabindex="-1" role="dialog" data-backdrop="false">
    <div class="modal-dialog {{ $size }}" role="document" style="{{ (isset($style)) ? $style : '' }}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                <h4 class="modal-title" id="{{ $modalId }}Label">{{ $modalTitle }}</h4>
            </div>
            <div id="modal-dynamic-content" name="modal-dynamic-content"></div>
        </div>
    </div>
</div>
<div class="modal-body">
    <div class="alert alert-danger" role="alert" style="margin-top: 1em; margin-bottom: 0.5em;">
        <strong>{{ $message }}</strong>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('app.close')</button>
</div>
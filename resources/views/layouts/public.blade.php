@extends('layouts.master')

@section('module_skin', 'skin-black-light')

@section('module_layout', 'layout-top-nav')

@section('styles')
    @parent
@endsection

@section('main_header')
    {{--@include('layouts.partials.mainheader')--}}
@endsection

@section('sidebar')
@endsection

@section('content_header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>@yield('content_header_title', 'HotelBot')<small>@yield('content_header_description')</small><div class="pull-right">@yield('content_header_extras')</div></h1>
        @include('layouts.partials.alerts')
    </section>
@endsection

@section('footer')
    {{--@include('layouts.partials.footer')--}}
@endsection

@section('scripts')
    @parent
@endsection
@section('main-content')
    <div class="box">
        <div class="box-body">
            <div class="form-group">
                <label for="hotel">Elija un hotel</label>
                <select id="hotel" class="hotelSelector form-control select2">
                    @if(!session('selectedHotelId'))
                        <option disabled selected value> -- Seleccione una opción -- </option>
                    @endif
                    @foreach($hoteles as $hot)
                        <option
                            @if($hot->id == session('selectedHotelId'))
                            selected
                            @endif
                            value="{{ $hot->id }}">
                            {{ $hot->nombre }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    @parent
@endsection

@section('scripts')
    @parent
    <script>
        $(function() {
            $('.hotelSelector').select2({
                language: "es"
            });

            $('#hotel').change(function(e){
                e.preventDefault();

                $.ajax({
                    url: "{{ route('hoteles.changeworkinghotel', [], false) }}",
                    type: "POST",
                    data: {
                        selectedHotelId: $(this).val(),
                    },
                    success: function () {
                        location.reload();
                    },
                    error: function(e){
                        console.log(e);
                    },
                });
            });
        });
    </script>
@endsection
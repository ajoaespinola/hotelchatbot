@extends('layouts.project')

@section('htmlheader_title', 'Hoteles')

@section('content_header_title')
    Hoteles
@endsection

@section('content_header_description')
    Crear nuevo hotel
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form action="{{ route('hoteles.store') }}" method="post" id="form_crear" name="form_crear">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="{{ old('nombre') }}">
                </div>
                <div class="form-group">
                    <label for="direccion">Dirección</label>
                    <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" value="{{ old('direccion') }}">
                </div>
                <div class="form-group">
                    <label for="telefono">Teléfono</label>
                    <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" value="{{ old('telefono') }}">
                </div>
                <div class="form-group">
                    <label for="extension_recepcion">Ext. Recepción</label>
                    <input type="text" class="form-control" id="extension_recepcion" name="extension_recepcion" placeholder="Extensión Recepción" value="{{ old('extension_recepcion') }}">
                </div>
                <div class="form-group">
                    <label for="hora_checkin">Inicio Check-In</label>
                    <input type="text" class="form-control" id="hora_checkin" name="hora_checkin" placeholder="Hora Inicio Check-In" value="{{ old('hora_checkin') }}">
                </div>
                <div class="form-group">
                    <label for="hora_checkout">Max Check-Out</label>
                    <input type="text" class="form-control" id="hora_checkout" name="hora_checkout" placeholder="Hora Máxima Check-Out" value="{{ old('hora_checkout') }}">
                </div>

                <button type="submit" class="btn btn-success btn-lg btn-block">Crear</button>
                <a class="btn btn-info btn-xs btn-block" href="{{ route('hoteles.index') }}" role="button">Retroceder</a>
            </form>
        </div>
    </div>
@endsection
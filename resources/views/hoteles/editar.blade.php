@extends('layouts.project')

@section('htmlheader_title', 'Hoteles')

@section('content_header_title')
    Hoteles
@endsection

@section('content_header_description')
    Editar hotel [<b>{{ $hotel->nombre }}</b>]
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form action="{{ route('hoteles.update', [$hotel]) }}" method="post" id="form_editar" name="form_editar">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="{{ old('nombre', $hotel->nombre) }}">
                </div>
                <div class="form-group">
                    <label for="direccion">Dirección</label>
                    <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" value="{{ old('direccion', $hotel->direccion) }}">
                </div>
                <div class="form-group">
                    <label for="telefono">Teléfono</label>
                    <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" value="{{ old('telefono', $hotel->telefono) }}">
                </div>
                <div class="form-group">
                    <label for="extension_recepcion">Ext. Recepción</label>
                    <input type="text" class="form-control" id="extension_recepcion" name="extension_recepcion" placeholder="Extensión Recepción" value="{{ old('extension_recepcion', $hotel->extension_recepcion) }}">
                </div>
                <div class="form-group">
                    <label for="hora_checkin">Inicio Check-In</label>
                    <input type="text" class="form-control" id="hora_checkin" name="hora_checkin" placeholder="Hora Inicio Check-In" value="{{ old('hora_checkin', ($hotel->hora_checkin) ? \Carbon\Carbon::createFromTimeString($hotel->hora_checkin)->format('H:i') : '') }}">
                </div>
                <div class="form-group">
                    <label for="hora_checkout">Max Check-Out</label>
                    <input type="text" class="form-control" id="hora_checkout" name="hora_checkout" placeholder="Hora Máxima Check-Out" value="{{ old('hora_checkout', ($hotel->hora_checkout) ? \Carbon\Carbon::createFromTimeString($hotel->hora_checkout)->format('H:i') : '') }}">
                </div>
                <button type="submit" class="btn btn-success btn-block"><i class="fa fa-save"></i> Guardar</button>
                <a class="btn btn-info btn-xs btn-block" href="{{ route('hoteles.show', [$hotel]) }}" role="button">Retroceder</a>
            </form>
        </div>
    </div>
@endsection
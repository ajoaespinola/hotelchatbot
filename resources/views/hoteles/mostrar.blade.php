@extends('layouts.project')

@section('htmlheader_title', "Detalles del hotel: $hotel->nombre")

@section('content_header_title')
    {{ $hotel->nombre }}
@endsection

@section('content_header_description')
    Detalles del hotel
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Datos básicos</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input disabled type="text" class="form-control" id="nombre" name="nombre" value="{{ $hotel->nombre }}">
                    </div>
                    <div class="form-group">
                        <label for="direccion">Dirección</label>
                        <input disabled type="text" class="form-control" id="direccion" name="direccion" value="{{ $hotel->direccion }}">
                    </div>
                    <div class="form-group">
                        <label for="telefono">Teléfono</label>
                        <input disabled type="text" class="form-control" id="telefono" name="telefono" value="{{ $hotel->telefono }}">
                    </div>
                    <div class="form-group">
                        <label for="extension_recepcion">Ext. Recepción</label>
                        <input disabled type="text" class="form-control" id="extension_recepcion" name="extension_recepcion" value="{{ $hotel->extension_recepcion }}">
                    </div>
                    @isset( $hotel->hora_checkin )
                        <div class="form-group">
                            <label for="hora_checkin">Inicio Check-In</label>
                            <input disabled type="text" class="form-control" id="hora_checkin" name="hora_checkin" value="{{ \Carbon\Carbon::createFromTimeString($hotel->hora_checkin)->format('H:i') }}">
                        </div>
                    @endisset
                    @isset( $hotel->hora_checkout )
                    <div class="form-group">
                        <label for="hora_checkout">Max Check-Out</label>
                        <input disabled type="text" class="form-control" id="hora_checkout" name="hora_checkout" value="{{ \Carbon\Carbon::createFromTimeString($hotel->hora_checkout)->format('H:i') }}">
                    </div>
                    @endisset
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a class="btn btn-warning btn-block" href="{{ route('hoteles.edit', [$hotel]) }}" role="button"><i class="fa fa-edit"></i> Editar</a>
                    <a class="btn btn-info btn-xs btn-block" href="{{ route('hoteles.index') }}" role="button">Retroceder</a>
                </div>
                <!-- box-footer -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

{{--
@section('scripts')
    @parent
    <script>
        $(function() {

        });
    </script>
@endsection
--}}

@extends('layouts.project')

@section('htmlheader_title', 'Hoteles')

@section('content_header_title')
    Hoteles
@endsection

@section('content_header_description')
    Listado de hoteles
@endsection

@section('content_header_extras')
    <a class="btn btn-primary" href="{{ route('hoteles.create') }}" role="button" title="Crear hotel"><i class="fa fa-plus-circle"></i> Crear hotel</a>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-body">
            @if (count($hoteles) > 0)
                <div class="table-responsive">
                    <table id="data_table" name="data_table" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th style="text-align: center; min-width: 30em;">Hotel</th>
                                <th style="text-align: center">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($hoteles as $hotel)
                            <tr style="text-align: center;">
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $hotel->nombre }}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <div class="btn-group" role="group">
                                        {{--Ver Registro--}}
                                        <a class="btn btn-xs btn-primary" href="{{ route('hoteles.show', [$hotel]) }}" role="button" title="Ver Detalles"><i class="fa fa-folder-open-o"></i> Ver Detalles</a>
                                        {{-- Editar registro --}}
                                        <a class="btn btn-xs btn-warning" href="{{ route('hoteles.edit', [$hotel]) }}" role="button" title="Editar registro"><i class="fa fa-edit"></i> Editar registro</a>
                                        {{-- Borrar registro --}}
                                        <form class="btn-group" action="{{ route('hoteles.destroy', [$hotel]) }}" method="post" onsubmit="return confirm('Confirma la eliminación de este registro?');">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-xs btn-danger" title="Borrar registro"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Borrar registro</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-warning" role="alert" style="margin-top: 0; margin-bottom: 0;"><b>Aún no se han creado hoteles en el sistema</b></div>
            @endif
        </div><!-- /.box-body -->
        <div class="box-footer">
            <a class="btn btn-primary pull-right" href="{{ route('hoteles.create') }}" role="button" title="Crear hotel"><i class="fa fa-plus-circle"></i> Crear hotel</a>
        </div><!-- box-footer -->
    </div><!-- /.box -->
@endsection
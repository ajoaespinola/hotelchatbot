@extends('layouts.auth')

@section('htmlheader_title')
    Iniciar sesión
@endsection

@section('content')
    <body class="hold-transition login-page">
        <div class="login-box">
            @include('layouts.partials.headerlogo')

            @include('layouts.partials.alerts')

            <div class="login-box-body">
                <p class="login-box-msg">Iniciar sesión</p>

                <form action="{{ url('/login') }}" method="post" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Correo Electrónico" id="email" name="email" value="{{ old('email') }}" autofocus/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Contraseña" id="password" name="password"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordar
                            </label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Iniciar sesión</button>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 5px;">
                        <div class="col-xs-12">
                            <a href="{{ url('/password/reset') }}">Olvidé mi contraseña</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @include('layouts.partials.scripts_auth')
    </body>
@endsection

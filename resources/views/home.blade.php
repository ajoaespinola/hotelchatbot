@extends('layouts.project')

@section('htmlheader_title', 'Inicio')

@section('content_header_title')
    HotelBot
@endsection

@section('content_header_description')
    Bienvenido {{ Auth::user()->name }}, selecciona una opción del panel para comenzar
@endsection

{{--@section('main-content')

@endsection--}}

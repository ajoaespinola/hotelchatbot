@extends('layouts.project')

@section('htmlheader_title', "{$habitacion->hotel->nombre} | Detalles de la habitación {$habitacion->numero}")

@section('content_header_title')
    {{ $habitacion->hotel->nombre }}
@endsection

@section('content_header_description')
    Detalles de la habitación {{ $habitacion->numero }}
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Datos básicos</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="hotel_id">Hotel</label>
                        <input disabled type="text" class="form-control" id="hotel_id" name="hotel_id" value="{{ $habitacion->hotel->nombre }}">
                    </div>
                    <div class="form-group">
                        <label for="estado_habitacion_id">Estado</label>
                        <input disabled type="text" class="form-control" id="estado_habitacion_id" name="estado_habitacion_id" value="{{ $habitacion->estado->nombre }}">
                    </div>
                    <div class="form-group">
                        <label for="numero">Número</label>
                        <input disabled type="text" class="form-control" id="numero" name="numero" value="{{ $habitacion->numero }}">
                    </div>
                    <div class="form-group">
                        <label for="extension_tel">Ext. Telefónica</label>
                        <input disabled type="text" class="form-control" id="extension_tel" name="extension_tel" value="{{ $habitacion->extension_tel }}">
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a class="btn btn-warning btn-block" href="{{ route('habitaciones.edit', [$habitacion]) }}" role="button"><i class="fa fa-edit"></i> Editar</a>
                    <a class="btn btn-info btn-xs btn-block" href="{{ route('habitaciones.index') }}" role="button">Retroceder</a>
                </div>
                <!-- box-footer -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

{{--
@section('scripts')
    @parent
    <script>
        $(function() {

        });
    </script>
@endsection
--}}

@extends('layouts.project')

@section('htmlheader_title', "{$selectedHotel->nombre} | Crear nueva habitación")

@section('content_header_title')
    {{ $selectedHotel->nombre }}
@endsection

@section('content_header_description')
    Crear nueva habitación
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form action="{{ route('habitaciones.store') }}" method="post" id="form_crear" name="form_crear">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="numero">Número Habitación</label>
                    <input type="text" class="form-control" id="numero" name="numero" placeholder="Número Habitación" value="{{ old('numero') }}">
                </div>
                <div class="form-group">
                    <label for="extension_tel">Ext. Telefónica</label>
                    <input type="text" class="form-control" id="extension_tel" name="extension_tel" placeholder="Ext. Telefónica" value="{{ old('extension_tel') }}">
                </div>

                <button type="submit" class="btn btn-success btn-lg btn-block">Crear</button>
                <a class="btn btn-info btn-xs btn-block" href="{{ route('habitaciones.index') }}" role="button">Retroceder</a>
            </form>
        </div>
    </div>
@endsection
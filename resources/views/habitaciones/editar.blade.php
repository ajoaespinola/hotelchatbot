@extends('layouts.project')

@section('htmlheader_title', "{$selectedHotel->nombre} | Editar habitación {$habitacion->numero}")

@section('content_header_title')
    {{ $selectedHotel->nombre }}
@endsection

@section('content_header_description')
    Editar habitación <b>{{ $habitacion->numero }}</b>
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form action="{{ route('habitaciones.update', [$habitacion]) }}" method="post" id="form_editar" name="form_editar">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="numero">Número Habitación</label>
                    <input type="text" class="form-control" id="numero" name="numero" placeholder="Número Habitación" value="{{ old('numero', $habitacion->numero) }}">
                </div>
                <div class="form-group">
                    <label for="extension_tel">Ext. Telefónica</label>
                    <input type="text" class="form-control" id="extension_tel" name="extension_tel" placeholder="Ext. Telefónica" value="{{ old('extension_tel', $habitacion->extension_tel) }}">
                </div>
                <button type="submit" class="btn btn-success btn-block"><i class="fa fa-save"></i> Guardar</button>
                <a class="btn btn-info btn-xs btn-block" href="{{ route('habitaciones.show', [$habitacion]) }}" role="button">Retroceder</a>
            </form>
        </div>
    </div>
@endsection
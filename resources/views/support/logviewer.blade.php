@extends('layouts.project')

@section('htmlheader_title', "Soporte a clientes")

@section('content_header_title')
    Historial de conversaciones
@endsection

@section('content_header_description')
    Soporte a clientes
@endsection

@section('main-content')

    <div class="col-md-12" style="margin-bottom: 10px">
        <div style="background-color: #ffffff">
            @if (count($chat) > 0)
                <table id="data_table" class="table table-striped">
                    <thead>
                    <tr>
                        <th style="text-align: center">Id</th>
                        <th style="text-align: center">Fecha y Hora</th>
                        <th style="text-align: center">De</th>
                        <th style="text-align: center">A</th>
                        <th style="text-align: center">Texto</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($chat as $ch)
                        <tr style="text-align: center">
                            <td style="text-align: center">{{ $ch->id }}</td>
                            <td style="text-align: center">{{ $ch->created_at->format('d/m/Y H:i:s') }}</td>
                            <td style="text-align: center">{{ $ch->emisor_nombre }}</td>
                            <td style="text-align: center">{{ $ch->receptor_nombre }}</td>
                            <td style="text-align: left">{{ $ch->texto }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-warning" role="alert">Aún no se han creado datos de este tipo...</div>
            @endif
        </div>

        <a class="btn btn-primary btn-xs btn-block" href="{{ url()->previous() }}" role="button">Retroceder</a>
    </div>
@endsection

{{--
@section('scripts')
    @parent
    <script>
        $(function() {

        });
    </script>
@endsection
--}}
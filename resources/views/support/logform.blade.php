@extends('layouts.project')

@section('htmlheader_title', "Soporte a clientes")

@section('content_header_title')
    Visor de conversaciones
@endsection

@section('content_header_description')
    Soporte a clientes
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="callout callout-info">
                <h4>Aviso</h4>
                <p>Ingrese el pin facilitado por el cliente en la ventana de talk.to para poder visualizar el historial.</p>
            </div>

            <form action="{{ route('soporte.show') }}" method="post" id="form_ver_conversaciones" name="form_ver_conversaciones">
                {{ csrf_field() }}
                <h2>Ingrese el pin: </h2>
                <div class="input-group">
                    <input id="botpin" name="botpin" type="text" class="form-control input-lg" value="{{ old('botpin') }}">
                    <span class="input-group-btn">
                        <button id="searchChat" name="searchChat" class="btn btn-info btn-lg" type="submit">BUSCAR</button>
                    </span>
                </div><!-- /input-group -->
            </form>
        </div>
    </div>
@endsection

{{--
@section('scripts')
    @parent
    <script>
        $(function() {

        });
    </script>
@endsection
--}}

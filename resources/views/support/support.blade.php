@extends('layouts.public')

@section('htmlheader_title', "Soporte a clientes")

@section('content_header')
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div align="center">
                <p><img id="logo" src="{{ asset('img/logo_tras_160.png') }}" alt="HotelBot" width="130em" height="130em"></p>
                <h2 style="margin-top: 10px; margin-bottom: 10px;">Bienvenido a HotelBot</h2>
                <p>Soporte a clientes</p>
                <hr>
            </div>

            <div class="callout callout-info">
                <h4>Aviso</h4>
                <p>Cópialo y pegalo en la ventana de chat que se encuentra debajo para comenzar la conversación con un agente de Atención a Clientes. ¡Muchas gracias!</p>
            </div>

            <h2>Tu pin es: </h2>
            <div class="input-group">
                <input id="botpin" name="botpin" type="text" class="form-control input-lg" value="{{$conversationId}}" readonly>
                <span class="input-group-btn">
                    <button id="copybutton" name="copybutton" class="btn btn-success btn-lg" type="button">COPIAR</button>
                </span>
            </div><!-- /input-group -->
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/58bf5ea493cfd35572056a05/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    <script>
        // Copy to clipboard example
        document.querySelector("#copybutton").onclick = function() {
            // Select the content
            document.querySelector("#botpin").select();
            // Copy to the clipboard
            document.execCommand('copy');
        };
    </script>
@endsection
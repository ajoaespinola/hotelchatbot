@extends('layouts.project')

@section('htmlheader_title', "{$reserva->habitacion->hotel->nombre} | Editar la reserva de la habitación {$reserva->habitacion->numero}")

@section('content_header_title')
    {{ $reserva->habitacion->hotel->nombre }}
@endsection

@section('content_header_description')
    Editar la reserva de la habitación {{ $reserva->habitacion->numero }}
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form action="{{ route('reservas.update', [$reserva]) }}" method="post" id="form_editar" name="form_editar">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="huesped_id">Huésped</label>
                    <select name="huesped_id" id="huesped_id" class="form-control">
                        @foreach($huespedes as $h)
                            <option
                                @if($h->id == old('huesped_id', $reserva->huesped_id))
                                selected
                                @endif
                                value="{{ $h->id }}">
                                {{ $h->nombre_completo }} ({{ $h->email  }})
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="fecha_inicio_fin">Fecha Inicio/Fin</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="fecha_inicio_fin" name="fecha_inicio_fin" placeholder="Fecha Inicio/Fin" readonly value="{{ old('fecha_inicio_fin', $reserva->fecha_inicio->format('d/m/Y') . ' - ' . $reserva->fecha_fin->format('d/m/Y')) }}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="obs">Observaciones</label>
                    <textarea class="form-control" rows="3" id="obs" name="obs" placeholder="Observaciones">{{ old('obs', $reserva->obs) }}</textarea>
                </div>
                <button type="submit" class="btn btn-success btn-block"><i class="fa fa-save"></i> Guardar</button>
                <a class="btn btn-info btn-xs btn-block" href="{{ route('reservas.show', [$reserva]) }}" role="button">Retroceder</a>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $(function() {
            $('#huesped_id').select2();

            $('input[name="fecha_inicio_fin"]').daterangepicker({
                minDate: '{{ \Carbon\Carbon::today()->format('d/m/Y') }}',
                autoUpdateInput: false,
                locale: {
                    format: "DD/MM/YYYY",
                    applyLabel: "Aplicar",
                    cancelLabel: 'Limpiar',
                    fromLabel: "Desde",
                    toLabel: "Hasta",
                    daysOfWeek: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"],
                    monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                },
            }).on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            }).on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
    </script>
@endsection
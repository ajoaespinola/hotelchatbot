@extends('layouts.project')

@section('htmlheader_title', "{$reserva->habitacion->hotel->nombre} | Detalles de la reserva de la habitación {$reserva->habitacion->numero}")

@section('content_header_title')
    {{ $reserva->habitacion->hotel->nombre }}
@endsection

@section('content_header_description')
    Detalles de la reserva de la habitación {{ $reserva->habitacion->numero }}
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Datos básicos</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4" style="text-align: center">
                            <h4>Habitación</h4>
                            <h3 style="margin-top: 0;">
                                <span class="label label-success">{{ $reserva->habitacion->numero }}</span>
                            </h3>
                        </div>
                        <div class="col-md-4" style="text-align: center">
                            <h4>Huésped</h4>
                            <h3 style="margin-top: 0;">
                                {{ $reserva->huesped->nombre_completo }}
                            </h3>
                        </div>
                        <div class="col-md-4" style="text-align: center">
                            <h4>Estado Reserva</h4>
                            <h3 style="margin-top: 0;">
                                {{ $reserva->estado->nombre }}
                            </h3>
                        </div>
                    </div>

                    <hr style="margin-top: 5px; margin-bottom: 5px;">

                    <div class="row">
                        <div class="col-md-6" style="text-align: center">
                            <h5>Fecha Inicio:<br><b>{{ $reserva->fecha_inicio->format('d/m/Y') }}</b></h5>
                        </div>
                        <div class="col-md-6" style="text-align: center">
                            <h5>Fecha Fin:<br><b>{{ $reserva->fecha_fin->format('d/m/Y') }}</b></h5>
                        </div>
                    </div>

                    @if( $reserva->obs )
                        <hr style="margin-top: 5px; margin-bottom: 5px;">

                        <div class="form-group">
                            <label for="obs">Observaciones</label>
                            <textarea disabled class="form-control" rows="3" id="obs" name="obs" placeholder="Observaciones">{{ $reserva->obs }}</textarea>
                        </div>
                    @endif
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Pin para Chatbot</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="text-align: center">
                            <h2 style="margin-top: 0;">
                                <span class="label label-success">{{ $reserva->bot_token }}</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->

            <a class="btn btn-warning btn-block" href="{{ route('reservas.edit', [$reserva]) }}" role="button"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-info btn-xs btn-block" href="{{ route('reservas.index') }}" role="button">Retroceder</a>
        </div>
    </div>
@endsection

{{--
@section('scripts')
    @parent
    <script>
        $(function() {

        });
    </script>
@endsection
--}}

@extends('layouts.project')

@section('htmlheader_title', 'Reservas')

@section('content_header_title')
    Reservas
@endsection

@section('content_header_description')
    Listado de reservas
@endsection

@section('main-content')
    @include('hoteles.partials.selector_lista_hoteles')

    @if(session('selectedHotelId'))
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Reservas registradas en {{ $selectedHotel->nombre }}</h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-primary" href="{{ route('reservas.create') }}" role="button" title="Crear reserva"><i class="fa fa-plus-circle"></i> Crear reserva</a>
                </div>
                <!-- /.box-tools -->
            </div>
            <div class="box-body">
                @if (count($reservas) > 0)
                    <div class="table-responsive">
                        <table id="data_table" name="data_table" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th style="text-align: center;">Número Habitación</th>
                                <th style="text-align: center;">Huésped</th>
                                <th style="text-align: center;">Estado Reserva</th>
                                <th style="text-align: center;">Fecha Inicio</th>
                                <th style="text-align: center;">Fecha Fin</th>
                                <th style="text-align: center;">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reservas as $reserva)
                                <tr style="text-align: center;">
                                    <td style="text-align: center; vertical-align: middle;">
                                        {{ $reserva->habitacion->numero }}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        {{ $reserva->huesped->nombre_completo }}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        {{ $reserva->estado->nombre }}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        {{ $reserva->fecha_inicio->format('d/m/Y') }}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        {{ $reserva->fecha_fin->format('d/m/Y') }}
                                    </td>
                                    <td style="text-align: right; vertical-align: middle;">
                                        <div class="btn-group" role="group">
                                            {{-- Ver Registro --}}
                                            <a class="btn btn-xs btn-primary" href="{{ route('reservas.show', [$reserva]) }}" role="button" title="Ver Detalles"><i class="fa fa-folder-open-o"></i> Ver Detalles</a>
                                            {{-- Borrar registro --}}
                                            <form class="btn-group" action="{{ route('reservas.destroy', [$reserva]) }}" method="post" onsubmit="return confirm('Confirma la eliminación de este registro?');">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-xs btn-danger" title="Borrar registro"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Borrar registro</button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-warning" role="alert" style="margin-top: 0; margin-bottom: 0;"><b>Aún no se han creado reservas en el hotel seleccionado</b></div>
                @endif
            </div><!-- /.box-body -->
            <div class="box-footer">
                <a class="btn btn-primary pull-right" href="{{ route('reservas.create') }}" role="button" title="Crear reservas"><i class="fa fa-plus-circle"></i> Crear reservas</a>
            </div><!-- box-footer -->
        </div><!-- /.box -->
    @else
        <div class="callout callout-info">
            <h4>Aviso</h4>
            <p>Debe seleccionar un hotel para poder ver el listado de reservas del mismo</p>
        </div>
    @endif
@endsection
@extends('layouts.project')

@section('htmlheader_title', "{$selectedHotel->nombre} | Crear nueva reserva")

@section('content_header_title')
    {{ $selectedHotel->nombre }}
@endsection

@section('content_header_description')
    Crear nueva reserva
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form action="{{ route('reservas.store') }}" method="post" id="form_crear" name="form_crear">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="habitacion_id">Habitación</label>
                    <select name="habitacion_id" id="habitacion_id" class="form-control">
                        @foreach($habitaciones as $h)
                            <option value="{{ $h->id }}">
                                {{ $h->numero }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="huesped_id">Huésped</label>
                    <select name="huesped_id" id="huesped_id" class="form-control">
                        @foreach($huespedes as $h)
                            <option value="{{ $h->id }}">
                                {{ $h->nombre_completo }} ({{ $h->email  }})
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="fecha_inicio_fin">Fecha Inicio/Fin</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="fecha_inicio_fin" name="fecha_inicio_fin" placeholder="Fecha Inicio/Fin" readonly value="{{ old('fecha_inicio_fin') }}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="obs">Observaciones</label>
                    <textarea class="form-control" rows="3" id="obs" name="obs" placeholder="Observaciones">{{ old('obs') }}</textarea>
                </div>

                <button type="submit" class="btn btn-success btn-lg btn-block">Crear</button>
                <a class="btn btn-info btn-xs btn-block" href="{{ route('reservas.index') }}" role="button">Retroceder</a>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $(function() {
            $('#habitacion_id').select2();
            $('#huesped_id').select2();

            $('input[name="fecha_inicio_fin"]').daterangepicker({
                minDate: '{{ \Carbon\Carbon::today()->format('d/m/Y') }}',
                autoUpdateInput: false,
                locale: {
                    format: "DD/MM/YYYY",
                    applyLabel: "Aplicar",
                    cancelLabel: 'Limpiar',
                    fromLabel: "Desde",
                    toLabel: "Hasta",
                    daysOfWeek: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"],
                    monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                },
            }).on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            }).on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
    </script>
@endsection
<?php

use Illuminate\Database\Seeder;

class EstadoReservaMesasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Restaurant\EstadoReservaMesa::create([
            "id" => 1,
            "nombre" => "Activa",
        ]);

        \App\Models\Restaurant\EstadoReservaMesa::create([
            "id" => 2,
            "nombre" => "Inactiva",
        ]);
    }
}

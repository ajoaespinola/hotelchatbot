<?php

use Illuminate\Database\Seeder;

class HabitacionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Hotel\Habitacion::create([
            "hotel_id" => 1,
            "estado_habitacion_id" => 1,
            "numero" => "101",
            "extension_tel" => 1001,
        ]);

        \App\Models\Hotel\Habitacion::create([
            "hotel_id" => 1,
            "estado_habitacion_id" => 1,
            "numero" => "102",
            "extension_tel" => 1002,
        ]);

        \App\Models\Hotel\Habitacion::create([
            "hotel_id" => 1,
            "estado_habitacion_id" => 1,
            "numero" => "103",
            "extension_tel" => 1003,
        ]);

        \App\Models\Hotel\Habitacion::create([
            "hotel_id" => 2,
            "estado_habitacion_id" => 1,
            "numero" => "1A",
            "extension_tel" => 001,
        ]);

        \App\Models\Hotel\Habitacion::create([
            "hotel_id" => 2,
            "estado_habitacion_id" => 1,
            "numero" => "2A",
            "extension_tel" => 002,
        ]);

        \App\Models\Hotel\Habitacion::create([
            "hotel_id" => 2,
            "estado_habitacion_id" => 1,
            "numero" => "3A",
            "extension_tel" => 003,
        ]);
    }
}

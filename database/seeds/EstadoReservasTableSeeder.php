<?php

use Illuminate\Database\Seeder;

class EstadoReservasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Reserva\EstadoReserva::create([
            "id" => 1,
            "nombre" => "Activa",
        ]);

        \App\Models\Reserva\EstadoReserva::create([
            "id" => 2,
            "nombre" => "Inactiva",
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class HuespedesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Hotel\Huesped::create([
            "hotel_id" => 1,
            "nombre" => "Aldo",
            "apellido" => "Espinola",
            "email" => "aldo@espino.la",
        ]);

        factory(\App\Models\Hotel\Huesped::class, 50)->create();
    }
}

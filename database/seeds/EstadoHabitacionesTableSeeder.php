<?php

use Illuminate\Database\Seeder;

class EstadoHabitacionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Hotel\EstadoHabitacion::create([
            "id" => 1,
            "nombre" => "Libre",
        ]);

        \App\Models\Hotel\EstadoHabitacion::create([
            "id" => 2,
            "nombre" => "Ocupada",
        ]);
    }
}

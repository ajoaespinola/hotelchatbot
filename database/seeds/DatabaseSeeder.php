<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);

        $this->call(HotelesTableSeeder::class);
        $this->call(EstadoHabitacionesTableSeeder::class);
        $this->call(HabitacionesTableSeeder::class);
        $this->call(HuespedesTableSeeder::class);

        $this->call(EstadoReservasTableSeeder::class);

        $this->call(EstadoDespertadoreTableSeeder::class);

        $this->call(EstadoLimpiezaHabitacionesTableSeeder::class);
        $this->call(HorarioLimpiezaHabitacionesTableSeeder::class);

        $this->call(RestaurantesTableSeeder::class);
        $this->call(EstadoReservaMesasTableSeeder::class);
    }
}

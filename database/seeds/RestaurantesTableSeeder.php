<?php

use Illuminate\Database\Seeder;

class RestaurantesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Restaurant\Restaurant::create([
            "id" => 1,
            "hotel_id" => 1,
            "nombre" => "O Gaucho",
            "descripcion" => "Inspirado en el Gaucho Brasileño ofrece los mejores cortes de carne y los platos más sabrosos en su Buffet. O Gaucho se distingue por ser una Parrilla y una Churrasqueria de cocina Internacional donde podes Almorzar y Cenar.",
            "foto" => "restaurant_1.jpg",
            "hora_apertura" => "05:00:00",
            "hora_cierre" => "03:00:00",
        ]);

        \App\Models\Restaurant\Restaurant::create([
            "id" => 2,
            "hotel_id" => 1,
            "nombre" => "Los Leños",
            "descripcion" => "Los Leños es un restaurante que fusiona lo clásico con lo moderno, además de ser 100% gluten free. Abrimos en abril de 1981 siendo un local fundamental de la historia gastronómica local, en donde creamos un espacio cálido y cómodo, que invita a compartir.",
            "foto" => "restaurant_2.jpg",
            "hora_apertura" => "05:00:00",
            "hora_cierre" => "03:00:00",
        ]);

        \App\Models\Restaurant\Restaurant::create([
            "id" => 3,
            "hotel_id" => 2,
            "nombre" => "La Perdiz",
            "descripcion" => "Es nuestro restaurante de autor con una variedad de estilos culinarios. El delicioso menú presenta tanto platos típicos locales como cocina internacional.",
            "foto" => "restaurant_3.jpg",
            "hora_apertura" => "05:00:00",
            "hora_cierre" => "03:00:00",
        ]);
    }
}

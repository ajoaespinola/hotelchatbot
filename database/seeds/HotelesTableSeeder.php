<?php

use Illuminate\Database\Seeder;

class HotelesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Hotel\Hotel::create([
            "id" => 1,
            "nombre" => "Hotel 1",
            "direccion" => "Coronel Ángel López N° 1012",
            "telefono" => "+598 01 100 200 / +598 01 200 300",
            "extension_recepcion" => 0,
            "hora_checkin" => "09:00:00",
            "hora_checkout" => "12:00:00",
        ]);

        \App\Models\Hotel\Hotel::create([
            "id" => 2,
            "nombre" => "Hotel 2",
            "direccion" => "Luis Piera N° 1992",
            "telefono" => "+598 01 100 200 / +598 01 200 300",
            "extension_recepcion" => 999,
            "hora_checkin" => "09:00:00",
            "hora_checkout" => "12:00:00",
        ]);
    }
}

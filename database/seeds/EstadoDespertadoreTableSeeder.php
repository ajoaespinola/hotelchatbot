<?php

use Illuminate\Database\Seeder;

class EstadoDespertadoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Servicio\EstadoDespertador::create([
            "id" => 1,
            "nombre" => "Pendiente",
        ]);

        \App\Models\Servicio\EstadoDespertador::create([
            "id" => 2,
            "nombre" => "Finalizado",
        ]);
    }
}

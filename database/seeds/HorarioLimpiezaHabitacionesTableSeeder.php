<?php

use Illuminate\Database\Seeder;

class HorarioLimpiezaHabitacionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Servicio\HorarioLimpiezaHabitacion::create([
            "id" => 1,
            "hotel_id" => 1,
            "nombre" => "Turno Mañana",
            "hora_inicio" => "09:00:00",
            "hora_fin" => "13:00:00"
        ]);

        \App\Models\Servicio\HorarioLimpiezaHabitacion::create([
            "id" => 2,
            "hotel_id" => 1,
            "nombre" => "Turno Tarde",
            "hora_inicio" => "13:00:00",
            "hora_fin" => "17:00:00"
        ]);

        \App\Models\Servicio\HorarioLimpiezaHabitacion::create([
            "id" => 3,
            "hotel_id" => 1,
            "nombre" => "Turno Noche",
            "hora_inicio" => "17:00:00",
            "hora_fin" => "21:00:00"
        ]);

        \App\Models\Servicio\HorarioLimpiezaHabitacion::create([
            "id" => 4,
            "hotel_id" => 2,
            "nombre" => "Turno Mañana",
            "hora_inicio" => "09:00:00",
            "hora_fin" => "13:00:00"
        ]);

        \App\Models\Servicio\HorarioLimpiezaHabitacion::create([
            "id" => 5,
            "hotel_id" => 2,
            "nombre" => "Turno Tarde",
            "hora_inicio" => "13:00:00",
            "hora_fin" => "17:00:00"
        ]);

        \App\Models\Servicio\HorarioLimpiezaHabitacion::create([
            "id" => 6,
            "hotel_id" => 2,
            "nombre" => "Turno Noche",
            "hora_inicio" => "17:00:00",
            "hora_fin" => "21:00:00"
        ]);
    }
}

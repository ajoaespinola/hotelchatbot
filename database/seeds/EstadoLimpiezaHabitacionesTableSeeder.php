<?php

use Illuminate\Database\Seeder;

class EstadoLimpiezaHabitacionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Servicio\EstadoLimpiezaHabitacion::create([
            "id" => 1,
            "nombre" => "Pendiente",
        ]);

        \App\Models\Servicio\EstadoLimpiezaHabitacion::create([
            "id" => 2,
            "nombre" => "Finalizado",
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBotConversacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bot_conversaciones', function (Blueprint $table) {
            $table->increments('id');

            $table->string('canal_id');
            $table->string('conversacion_id')->unique();

            $table->string('token')->nullable();
            $table->boolean('token_validado')->default(false);

            $table->json('propiedades')->nullable();

            $table->timestamps();
        });

        Schema::create('bot_conversacion_detalles', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('bot_conversacion_id');
            $table->foreign('bot_conversacion_id')
                ->references('id')
                ->on('bot_conversaciones')
                ->onDelete('cascade');

            $table->string('emisor_id', 60); // from_id
            $table->string('emisor_nombre', 40)->nullable(); // from_name
            $table->string('receptor_id', 60); // recipient_id
            $table->string('receptor_nombre', 40)->nullable(); // recipient_name

            $table->longText('texto');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bot_conversacion_detalles');
        Schema::dropIfExists('bot_conversaciones');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurantes', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('hotel_id');
            $table->foreign('hotel_id')
                ->references('id')
                ->on('hoteles')
                ->onDelete('cascade');

            $table->string('nombre');
            $table->string('descripcion');
            $table->string('foto');

            $table->time('hora_apertura');
            $table->time('hora_cierre');

            $table->timestamps();
        });

        Schema::create('estado_res_mesas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });

        Schema::create('res_mesas', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('reserva_id');
            $table->foreign('reserva_id')
                ->references('id')
                ->on('reservas')
                ->onDelete('cascade');

            $table->unsignedInteger('restaurante_id');
            $table->foreign('restaurante_id')
                ->references('id')
                ->on('restaurantes')
                ->onDelete('cascade');

            $table->unsignedInteger('cantidad_personas');
            $table->date('fecha_reserva');
            $table->time('hora_llegada');
            $table->string('obs')->nullable();

            $table->unsignedInteger('estado_res_mesa_id');
            $table->foreign('estado_res_mesa_id')
                ->references('id')
                ->on('estado_res_mesas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('res_mesas');
        Schema::dropIfExists('estado_res_mesas');
        Schema::dropIfExists('restaurantes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estado_reservas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });

        Schema::create('reservas', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('habitacion_id');
            $table->foreign('habitacion_id')
                ->references('id')
                ->on('habitaciones')
                ->onDelete('cascade');

            $table->unsignedInteger('huesped_id');
            $table->foreign('huesped_id')
                ->references('id')
                ->on('huespedes')
                ->onDelete('cascade');

            $table->unsignedInteger('estado_reserva_id');
            $table->foreign('estado_reserva_id')
                ->references('id')
                ->on('estado_reservas');

            $table->date('fecha_inicio');
            $table->date('fecha_fin');

            $table->string('bot_token')->unique();

            $table->longText('obs')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
        Schema::dropIfExists('estado_reservas');
    }
}

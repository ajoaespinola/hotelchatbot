<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLimpiezaHabitacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estado_limpieza_habs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });

        Schema::create('horario_limpieza_habs', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('hotel_id');
            $table->foreign('hotel_id')
                ->references('id')
                ->on('hoteles')
                ->onDelete('cascade');

            $table->string('nombre');

            $table->time('hora_inicio');
            $table->time('hora_fin');

            $table->timestamps();
        });

        Schema::create('limpieza_habs', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('reserva_id');
            $table->foreign('reserva_id')
                ->references('id')
                ->on('reservas')
                ->onDelete('cascade');

            $table->unsignedInteger('horario_limpieza_hab_id');
            $table->foreign('horario_limpieza_hab_id')
                ->references('id')
                ->on('horario_limpieza_habs');

            $table->unsignedInteger('estado_limpieza_hab_id');
            $table->foreign('estado_limpieza_hab_id')
                ->references('id')
                ->on('estado_limpieza_habs');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('limpieza_habs');
        Schema::dropIfExists('horario_limpieza_habs');
        Schema::dropIfExists('estado_limpieza_habs');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHuespedesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('huespedes', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('hotel_id');
            $table->foreign('hotel_id')
                ->references('id')
                ->on('hoteles')
                ->onDelete('cascade');

            $table->string('nombre');
            $table->string('apellido');
            $table->string('email')->unique();
            $table->longText('obs')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('huespedes');
    }
}

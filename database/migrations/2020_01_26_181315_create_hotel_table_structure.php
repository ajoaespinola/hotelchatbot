<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelTableStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoteles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('direccion');
            $table->string('telefono');

            $table->unsignedInteger('extension_recepcion');

            $table->time('hora_checkin')->nullable();
            $table->time('hora_checkout')->nullable();
            $table->timestamps();
        });

        Schema::create('estado_habitaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });

        Schema::create('habitaciones', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('hotel_id');
            $table->foreign('hotel_id')
                ->references('id')
                ->on('hoteles')
                ->onDelete('cascade');

            $table->unsignedInteger('estado_habitacion_id');
            $table->foreign('estado_habitacion_id')
                ->references('id')
                ->on('estado_habitaciones');

            $table->string('numero');

            $table->unsignedInteger('extension_tel');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habitaciones');
        Schema::dropIfExists('estado_habitaciones');
        Schema::dropIfExists('hoteles');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespertadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estado_despertadores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });

        Schema::create('despertadores', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('reserva_id');
            $table->foreign('reserva_id')
                ->references('id')
                ->on('reservas')
                ->onDelete('cascade');

            $table->time('hora_despertador');

            $table->unsignedInteger('estado_despertador_id');
            $table->foreign('estado_despertador_id')
                ->references('id')
                ->on('estado_despertadores');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('despertadores');
        Schema::dropIfExists('estado_despertadores');
    }
}

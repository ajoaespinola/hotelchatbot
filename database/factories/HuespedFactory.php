<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Hotel\Huesped::class, function (Faker $faker) {
    return [
        'hotel_id' => $faker->numberBetween(1, 2),
        'nombre' => $faker->firstName,
        'apellido' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
    ];
});

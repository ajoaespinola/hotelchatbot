<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'hotelchatbot');

// Project repository
set('repository', 'git@bitbucket.org:ajoaespinola/hotelchatbot.git');

set('writable_mode', 'chmod');
set('writable_use_sudo', 'true');
set('writable_chmod_mode', '777');
set('http_user', 'www-data');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);


// Hosts
set('default_stage', 'production');
host('production')
    ->hostname('159.203.98.8')
    ->port(22)
    ->stage('production')
    ->user('ajoa')
    ->set('deploy_path', '/var/www/hotelbot.espino.la');
    
// Tasks
desc('Execute artisan config:clear');
task('artisan:config:clear', function () {
    run('{{bin/php}} {{release_path}}/artisan config:clear');
});

before('cleanup', 'artisan:config:clear');

/*task('build', function () {
    run('cd {{release_path}} && build');
});*/

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

// before('deploy:symlink', 'artisan:migrate');


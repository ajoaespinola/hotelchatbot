<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('terminos', function () {
    return view('terminos');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['prefix' => 'hoteles', 'as' => 'hoteles.'], function () {
        Route::get('/', 'HotelController@index')->name('index');
        Route::get('create', 'HotelController@create')->name('create');
        Route::post('/', 'HotelController@store')->name('store');
        Route::get('{hotel}', 'HotelController@show')->name('show');
        Route::get('{hotel}/edit', 'HotelController@edit')->name('edit');
        Route::put('{hotel}', 'HotelController@update')->name('update');
        Route::delete('{hotel}', 'HotelController@destroy')->name('destroy');

        Route::post('changeWorkingHotel', 'HotelController@changeWorkingHotel')->name('changeworkinghotel');
    });

    Route::group(['prefix' => 'habitaciones', 'as' => 'habitaciones.'], function () {
        Route::get('/', 'HabitacionesController@index')->name('index');
        Route::get('create', 'HabitacionesController@create')->name('create');
        Route::post('/', 'HabitacionesController@store')->name('store');
        Route::get('{habitacion}', 'HabitacionesController@show')->name('show');
        Route::get('{habitacion}/edit', 'HabitacionesController@edit')->name('edit');
        Route::put('{habitacion}', 'HabitacionesController@update')->name('update');
        Route::delete('{habitacion}', 'HabitacionesController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'huespedes', 'as' => 'huespedes.'], function () {
        Route::get('/', 'HuespedesController@index')->name('index');
        Route::get('create', 'HuespedesController@create')->name('create');
        Route::post('/', 'HuespedesController@store')->name('store');
        Route::get('{huesped}', 'HuespedesController@show')->name('show');
        Route::get('{huesped}/edit', 'HuespedesController@edit')->name('edit');
        Route::put('{huesped}', 'HuespedesController@update')->name('update');
        Route::delete('{huesped}', 'HuespedesController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'reservas', 'as' => 'reservas.'], function () {
        Route::get('/', 'ReservasController@index')->name('index');
        Route::get('create', 'ReservasController@create')->name('create');
        Route::post('/', 'ReservasController@store')->name('store');
        Route::get('{reserva}', 'ReservasController@show')->name('show');
        Route::get('{reserva}/edit', 'ReservasController@edit')->name('edit');
        Route::put('{reserva}', 'ReservasController@update')->name('update');
        Route::delete('{reserva}', 'ReservasController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'soporte', 'as' => 'soporte.'], function () {
        // DERIVACION DE CONVERSACIONES
            // EL DE ATENCION A CLIENTES DEBE ENTRAR A ESTA PANTALLA
            Route::get('chats/viewer', function () {
                return view('support.logform');
            })->name('index');

            Route::post('chats/viewer', 'BotFwrk\ConversationController@showChatLog')->name('show');
        // DERIVACION DE CONVERSACIONES
    });
});

// DERIVACION DE CONVERSACIONES
    // EL CLIENTE VE ESTA PANTALLA
    Route::get('hotel/soporte/{conversationId}', function ($conversationId) {
        return view('support.support')->with('conversationId', $conversationId);
    });
// DERIVACION DE CONVERSACIONES

<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('hoteles.partials.selector_lista_hoteles', 'App\Http\ViewComposers\HotelSelectorList');

        // En todas las vistas cargo los datos del hotel seleccionado
        View::composer([
            'habitaciones.*',
            'huespedes.*',
            'reservas.*'
        ], 'App\Http\ViewComposers\GetSelectedHotelData');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

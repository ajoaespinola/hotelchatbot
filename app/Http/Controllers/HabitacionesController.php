<?php

namespace App\Http\Controllers;

use App\Models\Hotel\Habitacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HabitacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        if (session('selectedHotelId')){
            $habitaciones = Habitacion::with('estado')->where('hotel_id', session('selectedHotelId'))->get();

            return view('habitaciones.index', compact('habitaciones'));
        }

        return view('habitaciones.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('habitaciones.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'numero' => 'required|string',
            'extension_tel' => 'required|integer',
        ]);

        try {
            DB::beginTransaction();

            $habitacion = new Habitacion();

            $habitacion->hotel_id = session('selectedHotelId');
            $habitacion->extension_tel = $request->extension_tel;
            $habitacion->estado_habitacion_id = 1; // por defecto la habitacion nueva esta libre
            $habitacion->numero = $request->numero;

            $habitacion->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            \Session::flash("success", "La habitación fue creada exitosamente.");
            return redirect()->route('habitaciones.show', [$habitacion]);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Habitacion $habitacion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show(Habitacion $habitacion)
    {
        return view('habitaciones.mostrar', compact('habitacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Habitacion $habitacion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(Habitacion $habitacion)
    {
        return view('habitaciones.editar', compact('habitacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Habitacion $habitacion
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, Habitacion $habitacion)
    {
        $this->validate($request, [
            'numero' => 'required',
            'extension_tel' => 'required|integer',
        ]);

        try {
            DB::beginTransaction();

            $habitacion->numero = $request->numero;
            $habitacion->extension_tel = $request->extension_tel;

            $habitacion->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            \Session::flash("success", "La habitación fue editada exitosamente.");
            return redirect()->route('habitaciones.show', [$habitacion]);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Habitacion $habitacion
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy(Habitacion $habitacion)
    {
        try {
            DB::beginTransaction();

            $habitacion->delete();

            DB::commit();

            \Session::flash("success", "La habitación fue borrada exitosamente.");
            return redirect()->route('habitaciones.index');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}

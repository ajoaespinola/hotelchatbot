<?php

namespace App\Http\Controllers;

use App\Models\Servicio\LimpiezaHabitacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LimpiezaHabitacionController extends Controller
{
    public static function bfCreate($reserva_id, $horario_limpieza_hab_id)
    {
        try {
            DB::beginTransaction();

            $servicioLimpieza = new LimpiezaHabitacion();

            $servicioLimpieza->reserva_id = $reserva_id;
            $servicioLimpieza->horario_limpieza_hab_id = $horario_limpieza_hab_id;
            $servicioLimpieza->estado_limpieza_hab_id = 1; // pendiente por defecto

            $servicioLimpieza->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            return $servicioLimpieza;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(">> LimpiezaHabitacionController -- ERROR: " . $e->getMessage());
            return false;
        }
    }
}

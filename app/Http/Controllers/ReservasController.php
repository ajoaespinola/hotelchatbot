<?php

namespace App\Http\Controllers;

use App\Models\Hotel\Habitacion;
use App\Models\Hotel\Huesped;
use App\Models\Reserva\Reserva;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReservasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        if (session('selectedHotelId')){
            $reservas = Reserva::with([
                'huesped',
                'habitacion',
                'estado',
            ])->whereHas('habitacion', function ($query) {
                $query->where('hotel_id', session('selectedHotelId'));
            })->get();

            return view('reservas.index', compact('reservas'));
        }

        return view('reservas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $habitaciones = Habitacion::where('hotel_id', session('selectedHotelId'))
            ->where('estado_habitacion_id', 1) // solo habitaciones libres
            ->get();

        if ($habitaciones->isEmpty()) {
            \Session::flash("error", "No existen habitaciones disponibles para reserva.");
            return redirect()->back();
        }

        $huespedes = Huesped::where('hotel_id', session('selectedHotelId'))->get();

        return view('reservas.crear', compact('habitaciones', 'huespedes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'habitacion_id' => 'required|integer',
            'huesped_id' => 'required|integer',
            'fecha_inicio_fin' => 'required',
            'obs' => 'nullable|string',
        ]);

        try {
            DB::beginTransaction();

            // se cambia el estado de la habitacion a ocupada
            $habitacion = Habitacion::find($request->habitacion_id);
            $habitacion->estado_habitacion_id = 2;
            $habitacion->save();

            // se crea la reserva
            $reserva = new Reserva();

            $reserva->habitacion_id = $request->habitacion_id;
            $reserva->huesped_id = $request->huesped_id;

            $reserva->estado_reserva_id = 1; // activa por defecto

            $fechas = explode(' - ', $request->fecha_inicio_fin);
            $reserva->fecha_inicio = Carbon::createFromFormat('d/m/Y', $fechas[0])->format('Y-m-d');
            $reserva->fecha_fin = Carbon::createFromFormat('d/m/Y', $fechas[1])->format('Y-m-d');

            $reserva->bot_token = \Token::unique('reservas', 'bot_token', 8);

            if ($request->filled('obs')) {
                $reserva->obs = $request->obs;
            }

            $reserva->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            \Session::flash("success", "La reserva fue creada exitosamente.");
            return redirect()->route('reservas.show', [$reserva]);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($id)
    {
        $reserva = Reserva::with([
            'habitacion.hotel',
            'huesped'
        ])->find($id);

        return view('reservas.mostrar', compact('reserva'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Reserva $reserva
     * @return \Illuminate\Http\Response
     */
    public function edit(Reserva $reserva)
    {
        $huespedes = Huesped::where('hotel_id', session('selectedHotelId'))->get();

        return view('reservas.editar', compact('reserva', 'huespedes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Reserva $reserva
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reserva $reserva)
    {
        $this->validate($request, [
            'huesped_id' => 'required|integer',
            'fecha_inicio_fin' => 'required',
            'obs' => 'nullable|string',
        ]);

        try {
            DB::beginTransaction();

            $reserva->huesped_id = $request->huesped_id;

            $fechas = explode(' - ', $request->fecha_inicio_fin);
            $reserva->fecha_inicio = Carbon::createFromFormat('d/m/Y', $fechas[0])->format('Y-m-d');
            $reserva->fecha_fin = Carbon::createFromFormat('d/m/Y', $fechas[1])->format('Y-m-d');

            if ($request->filled('obs')) {
                $reserva->obs = $request->obs;
            } else {
                $reserva->obs = null;
            }

            $reserva->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            \Session::flash("success", "La reserva fue editada exitosamente.");
            return redirect()->route('reservas.show', [$reserva]);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Reserva $reserva
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy(Reserva $reserva)
    {
        try {
            DB::beginTransaction();

            if ($reserva->estado_reserva_id == 1) { // si es la reserva activa actual
                $reserva->habitacion->estado_habitacion_id = 1; // se libera la habitacion antes de borrar el registro
                $reserva->push();
            }

            $reserva->delete();

            DB::commit();

            \Session::flash("success", "La reserva fue borrada exitosamente.");
            return redirect()->route('reservas.index');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public static function getReservaByToken($token)
    {
        return Reserva::where('bot_token', $token)->first();
    }
}

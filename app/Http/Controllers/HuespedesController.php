<?php

namespace App\Http\Controllers;

use App\Models\Hotel\Huesped;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HuespedesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        if (session('selectedHotelId')){
            $huespedes = Huesped::where('hotel_id', session('selectedHotelId'))->get();

            return view('huespedes.index', compact('huespedes'));
        }

        return view('huespedes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('huespedes.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'apellido' => 'required',
            'email' => 'required|email|unique:huespedes,email', // check if the user is already taken
            'obs' => 'nullable',
        ]);

        try {
            DB::beginTransaction();

            $huesped = new Huesped();

            $huesped->hotel_id = session('selectedHotelId');
            $huesped->nombre = $request->nombre;
            $huesped->apellido = $request->apellido;
            $huesped->email = $request->email;
            if ($request->filled('obs')) {
                $huesped->obs = $request->obs;
            }

            $huesped->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            \Session::flash("success", "El huésped fue creado exitosamente.");
            return redirect()->route('huespedes.show', [$huesped]);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Huesped $huesped
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show(Huesped $huesped)
    {
        return view('huespedes.mostrar', compact('huesped'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Huesped $huesped
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(Huesped $huesped)
    {
        return view('huespedes.editar', compact('huesped'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Huesped $huesped
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, Huesped $huesped)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'apellido' => 'required',
            'obs' => 'nullable',
        ]);

        try {
            DB::beginTransaction();

            $huesped->nombre = $request->nombre;
            $huesped->apellido = $request->apellido;
            if ($request->filled('obs')) {
                $huesped->obs = $request->obs;
            } else {
                $huesped->obs = null;
            }

            $huesped->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            \Session::flash("success", "El huesped fue editado exitosamente.");
            return redirect()->route('huespedes.show', [$huesped]);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Huesped $huesped
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy(Huesped $huesped)
    {
        try {
            DB::beginTransaction();

            $huesped->delete();

            DB::commit();

            \Session::flash("success", "El huesped fue borrado exitosamente.");
            return redirect()->route('huespedes.index');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}

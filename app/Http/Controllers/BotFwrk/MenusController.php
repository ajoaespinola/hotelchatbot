<?php

namespace App\Http\Controllers\BotFwrk;

use App\Http\Controllers\BotFwrk\Helpers\Cards;
use App\Models\Reserva\Reserva;
use App\Models\Restaurant\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenusController extends Controller
{
    public static function sendMainMenu($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, Reserva $reserva) {
        $buttons = [
            [
                "type" => "postBack",
                "title" => "Servicios",
                "value" => "menu_ver_servicios"
            ],
            [
                "type" => "postBack",
                "title" => "Restaurant",
                "value" => "menu_ver_restaurantes"
            ],
            /*[
                "type" => "postBack",
                "title" => "Traslado",
                "value" => "C"
            ],*/
            [
                "type" => "postBack",
                "title" => "Informaciones Generales",
                "value" => "menu_ver_informaciones"
            ],
            /*[
                "type" => "postBack",
                "title" => "Eventos",
                "value" => "D"
            ],*/
            [
                "type" => "postBack",
                "title" => "Chat directo con personal del hotel",
                "value" => "derivar_conversacion"
            ]
        ];

        $title = "{$reserva->huesped->nombre_completo}";
        $text = "¡Sea bienvenido al {$reserva->habitacion->hotel->nombre}!.\n\nPuede seleccionar una opción del menú inferior o pruebe escribir directamente una petición.";
        $image = asset('img/hoteles/hotel_1.jpg');
        $imageDescription = "{$reserva->habitacion->hotel->nombre}";

        $cardToSend = Cards::prepareHeroCard($title, null, $text, $image, $imageDescription, $buttons);

        AnswerController::replyMessageWithAttachmentsToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, 'list', $cardToSend);
    }

    public static function sendServiciosMenu($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva) {
        $cardToSend[] = [ // el nombre de la variable debe de tener el [] al final para cumpli con la estructura de MSFT
            "contentType" => "application/vnd.microsoft.card.hero",
            "content" => array(
                "title" => "Servicios",
                "text" => "{$reserva->habitacion->hotel->nombre} le ofrece los siguientes servicios.\n\nPuede seleccionar una opción del menú inferior o pruebe escribir directamente una petición.",
                "buttons" => [
                    [
                        "type" => "postBack",
                        "title" => "Programar despertador",
                        "value" => "servicio_despertador"
                    ],
                    [
                        "type" => "postBack",
                        "title" => "Solicitar limpieza de la habitación",
                        "value" => "servicio_limpieza"
                    ]
                ]
            )
        ];
        AnswerController::replyMessageWithAttachmentsToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, 'list', $cardToSend);
    }

    public static function sendInformacionesMenu($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva) {
        $cardToSend[] = [ // el nombre de la variable debe de tener el [] al final para cumpli con la estructura de MSFT
            "contentType" => "application/vnd.microsoft.card.hero",
            "content" => array(
                "title" => "Información General",
                "text" => "Información sobre el establecimiento {$reserva->habitacion->hotel->nombre} y sobre su estadía.\n\nPuede seleccionar una opción del menú inferior o pruebe escribir directamente una petición.",
                "buttons" => [
                    [
                        "type" => "postBack",
                        "title" => "Dirección del hotel",
                        "value" => "info_direccion_hotel"
                    ],
                    [
                        "type" => "postBack",
                        "title" => "Teléfono del hotel",
                        "value" => "info_telefono_hotel"
                    ],
                    [
                        "type" => "postBack",
                        "title" => "Recibir llamadas en mi habitacion",
                        "value" => "info_telefono_habitacion"
                    ],
                    [
                        "type" => "postBack",
                        "title" => "Horario Check-In",
                        "value" => "info_checkin_hotel"
                    ],
                    [
                        "type" => "postBack",
                        "title" => "Horario Check-Out",
                        "value" => "info_checkout_hotel"
                    ],
                    [
                        "type" => "postBack",
                        "title" => "Mi Reserva",
                        "value" => "info_mi_reserva"
                    ]
                ]
            )
        ];

        AnswerController::replyMessageWithAttachmentsToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, 'list', $cardToSend);
    }

    public static function sendRestaurantesMenu($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva) {
        $response = "Dentro de sus instalaciones **{$reserva->habitacion->hotel->nombre}** posee los siguientes restaurantes para que deleite sus sentidos:";
        AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);


        $restaurantes = Restaurant::where('hotel_id', $reserva->habitacion->hotel->id)->get(); // traigo el listado de restaurantes del hotel

        $cardToSend = []; // ARRAY PADRE DEL CARRUSEL

        foreach ($restaurantes as $rest) {
            $cardToSend[] = [
                "contentType" => "application/vnd.microsoft.card.hero",
                "content" => [
                    "title" => $rest->nombre,
                    // "subtitle" => "",
                    "text" => $rest->descripcion,
                    "images" => [
                        [
                            "url" => asset("img/restaurantes/$rest->foto"),
                            "alt" => $rest->nombre
                        ]
                    ],
                    "buttons" => [
                        [
                            "type" => "postBack",
                            "title" => "RESERVAR",
                            "value" => "reservar mesa $rest->nombre"
                        ]
                    ]
                ]
            ];
        }

        AnswerController::replyMessageWithAttachmentsToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, 'carousel', $cardToSend);
    }
}

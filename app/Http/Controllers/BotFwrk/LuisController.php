<?php

namespace App\Http\Controllers\BotFwrk;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class LuisController extends Controller
{
    // LUIS - Language Understanding Intelligent Service
    // https://azure.microsoft.com/es-mx/services/cognitive-services/language-understanding-intelligent-service/
    // https://www.luis.ai
    public static function askLuis($query){
        Log::info("<><><><><><><><><><><><><><><><><><><> ASK LUIS");

        // https://portal.azure.com/
        // Panel "Más Servicios -> Cognitive Services accounts -> superexpressbot -> keys -> SELECCIONAR KEY"

        $http_client = new Client();
        try {
            $http_client_response = $http_client->request('GET', 'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/e8bf502c-14fb-4015-a66f-218a6be3bfc3', [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'query' => [
                    'verbose' => false,
                    'timezoneOffset' => -180,
                    'subscription-key' => '4cb42eb0eaf247ae9610b277bf8b33e7', // ajoa real azure api key
                    'q' => $query,
                ]
            ]);

            $http_client_response_body = $http_client_response->getBody()->getContents();

            $luisResponse = json_decode($http_client_response_body);
            // Log::debug(">> L.U.I.S. RESPONSE:\n" . json_encode($luisResponse, JSON_PRETTY_PRINT));

            Log::debug(">> askLuis <- BOT FWORK: Status [{$http_client_response->getStatusCode()}|{$http_client_response->getReasonPhrase()}] Respuesta: " . $http_client_response_body);

            return $luisResponse;
        } catch (RequestException $e) {
            Log::error(Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                Log::error(Psr7\str($e->getResponse()));
            }

            return null;
        }

        Log::info("<><><><><><><><><><><><><><><><><><><> ASK LUIS");
    }
}

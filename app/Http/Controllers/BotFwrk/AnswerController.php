<?php

namespace App\Http\Controllers\BotFwrk;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class AnswerController extends Controller
{
    public static function sendMessageToConversation($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $text)
    {
        Log::info("<><><><><><><><><><><><><><><><><><><> SEND MESSAGE TO CONVERSATION");

        $msft_access_token = AuthorizationController::getAccessToken();

        Log::debug(">> sendMessageToConversation TEXT TO SEND: [" . $text . "]");

        Log::info(">> sendMessageToConversation -> BOT FWORK: Enviando mensaje... ");

        $http_client = new Client();
        try {
            $http_client_response = $http_client->request('POST', rtrim($serviceUrl,"/")."/v3/conversations/$conversationId/activities", [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $msft_access_token
                ],
                'json' => [
                    'type' => 'message',
                    'locale' => 'es-PY',
                    'from' => [
                        'id' => "$recipientId",
                        'name' => "$recipientName",
                    ],
                    'conversation' => [
                        'id' => "$conversationId",
                        'name' => "",
                    ],
                    'recipient' => [
                        'id' => "$fromId",
                        'name' => "$fromName",
                    ],
                    'text' => "$text"
                ]
            ]);

            Log::debug(">> sendMessageToConversation <- BOT FWORK: Status [{$http_client_response->getStatusCode()}|{$http_client_response->getReasonPhrase()}] Respuesta: " . $http_client_response->getBody()->getContents());
        } catch (RequestException $e) {
            Log::error(Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                Log::error(Psr7\str($e->getResponse()));
            }
        }

        Log::info("<><><><><><><><><><><><><><><><><><><> SEND MESSAGE TO CONVERSATION");
    }

    public static function replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $text)
    {
        Log::info("<><><><><><><><><><><><><><><><><><><> REPLY MESSAGE TO ID");

        $msft_access_token = AuthorizationController::getAccessToken();

        Log::debug(">> replyMessageToId TEXT TO SEND: [" . $text . "]");

        Log::info(">> replyMessageToId -> BOT FWORK: Enviando mensaje... ");

        $http_client = new Client();
        try {
            $http_client_response = $http_client->request('POST', rtrim($serviceUrl,"/")."/v3/conversations/$conversationId/activities/$activityId", [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $msft_access_token
                ],
                'json' => [
                    'type' => 'message',
                    'locale' => 'es-PY',
                    'from' => [
                        'id' => $recipientId,
                        'name' => $recipientName,
                    ],
                    'conversation' => [
                        'id' => "$conversationId",
                        'name' => "",
                    ],
                    'recipient' => [
                        'id' => $fromId,
                        'name' => $fromName,
                    ],
                    'text' => "$text",
                    'replyToId' => $activityId
                ]
            ]);

            Log::debug(">> replyMessageToId <- BOT FWORK: Status [{$http_client_response->getStatusCode()}|{$http_client_response->getReasonPhrase()}] Respuesta: " . $http_client_response->getBody()->getContents());
        } catch (RequestException $e) {
            Log::error(Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                Log::error(Psr7\str($e->getResponse()));
            }
        }

        Log::info("<><><><><><><><><><><><><><><><><><><> REPLY MESSAGE TO ID");
    }

    public static function replyMessageWithAttachmentsToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $attachmentLayout, $attachments)
    {
        Log::info("<><><><><><><><><><><><><><><><><><><> REPLY MESSAGE WITH ATTACHMENTS TO ID");

        $msft_access_token = AuthorizationController::getAccessToken();

        Log::info(">> replyMessageWithAttachmentsToId -> BOT FWORK: Enviando mensaje... ");

        $http_client = new Client();
        try {
            $http_client_response = $http_client->request('POST', rtrim($serviceUrl,"/")."/v3/conversations/$conversationId/activities/$activityId", [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $msft_access_token
                ],
                'json' => [
                    'type' => 'message',
                    'locale' => 'es-PY',
                    'from' => [
                        'id' => $recipientId,
                        'name' => $recipientName,
                    ],
                    'conversation' => [
                        'id' => "$conversationId",
                        'name' => "",
                    ],
                    'recipient' => [
                        'id' => $fromId,
                        'name' => $fromName,
                    ],
                    'attachmentLayout' => $attachmentLayout,
                    'attachments' => $attachments,
                    'replyToId' => $activityId
                ]
            ]);

            Log::debug(">> replyMessageToId <- BOT FWORK: Status [{$http_client_response->getStatusCode()}|{$http_client_response->getReasonPhrase()}] Respuesta: " . $http_client_response->getBody()->getContents());
        } catch (RequestException $e) {
            Log::error(Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                Log::error(Psr7\str($e->getResponse()));
            }
        }

        Log::info("<><><><><><><><><><><><><><><><><><><> REPLY MESSAGE WITH ATTACHMENTS TO ID");
    }
}

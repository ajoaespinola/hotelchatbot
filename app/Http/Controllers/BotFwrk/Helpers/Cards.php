<?php


namespace App\Http\Controllers\BotFwrk\Helpers;


class Cards
{
    public static function prepareHeroCard($title, $subtitle, $text, $image, $imageDescripcion, $buttons){
        $card[] = [
            "contentType" => "application/vnd.microsoft.card.hero",
            "content" => [
                "title" => $title,
                "subtitle" => $subtitle,
                "text" => $text,
                "images" => [
                    [
                        "url" => $image,
                        "alt" => $imageDescripcion,
                    ]
                ],
                "buttons" => $buttons
            ]
        ];
        return $card;
    }

    public static function prepareHeroWithoutImgCard($title, $subtitle, $text, $buttons){
        $card[] = [
            "contentType" => "application/vnd.microsoft.card.hero",
            "content" => [
                "title" => $title,
                "subtitle" => $subtitle,
                "text" => $text,
                "buttons" => $buttons
            ]
        ];
        return $card;
    }

    public static function prepareBooleanConfirmationHeroCard($title, $subtitle, $text){
        $card[] = [
            "contentType" => "application/vnd.microsoft.card.hero",
            "content" => [
                "title" => $title,
                "subtitle" => $subtitle,
                "text" => $text,
                "buttons" => [
                    [
                        "type" => "postBack",
                        "title" => "CONFIRMAR",
                        "value" => "CONFIRMAR"
                    ],
                    [
                        "type" => "postBack",
                        "title" => "CANCELAR",
                        "value" => "CANCELAR"
                    ]
                ]
            ]
        ];
        return $card;
    }

    public static function prepareTextOneButtonHero($title, $subtitle, $text, $btnType, $btnValue, $btnUrl){
        $card[] = array(
            "contentType" => "application/vnd.microsoft.card.hero",
            "content" => [
                "title" => $title,
                "subtitle" => $subtitle,
                "text" => $text,
                "buttons" => [
                    [
                        "type" => $btnType,
                        "title" => $btnValue,
                        "value" => url($btnUrl)
                    ]
                ]
            ]
        );
        return $card;
    }
}
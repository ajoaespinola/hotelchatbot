<?php

namespace App\Http\Controllers\BotFwrk;

use App\Http\Controllers\ReservasController;
use App\Models\Reserva\Reserva;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class ListenerController extends Controller
{
    public function listener(Request $request)
    {
        // Log::debug("*** BOT FRAMEWORK REQUEST: " . $request);

        Log::info(" ");
        Log::info("***");
        Log::info(" ");
        Log::info("BOT FWORK -> HOTELBOT: INCOMING REQUEST! ** ");

        Log::info("<><><><><><><><><><><><><><><><><><><> PARAMETROS");
        $type = $request->input('type');
        Log::info("type: " . $type);
        $serviceUrl = $request->input('serviceUrl');
        Log::info("serviceUrl: " . $serviceUrl);
        $channelId = $request->input('channelId');
        Log::info("channelId: " . $channelId);
        $locale = $request->input('locale');
        Log::info("locale: " . $locale);

        $conversationId = $request->input('conversation.id');
        Log::info("conversationId: " . $conversationId);

        $activityId = $request->input('id');
        Log::info("activityId: " . $activityId);

        $fromId = $request->input('from.id');
        Log::info("fromId: " . $fromId);
        $fromName = $request->input('from.name');
        Log::info("fromName: " . $fromName);

        $recipientId = $request->input('recipient.id');
        Log::info("recipientId: " . $recipientId);
        $recipientName = $request->input('recipient.name');
        Log::info("recipientName: " . $recipientName);

        $text = $request->input('text');
        Log::info("text: " . $text);

        $localTimestamp = $request->input('localTimestamp');
        Log::info("localTimestamp: " . $localTimestamp);
        $timestamp = $request->input('timestamp');
        Log::info("timestamp: " . $timestamp);

        Log::info("<><><><><><><><><><><><><><><><><><><> PARAMETROS");

        switch ($type) {
            case 'conversationUpdate':
                /**
                 * The Bot Framework provides the conversationUpdate event for notifying your bot whenever
                 * a member joins or leaves a conversation. A conversation member can be a user or a bot.
                 */
                response("Aceptado", 202);
                $messageToSend = "¡Bienvenido/a {$fromName} a la plataforma de autogestión HOTELBOT!, por favor ingresa el código que te han dado en la recepción al momento del check-in para comenzar.";
                AnswerController::sendMessageToConversation($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $messageToSend);
            break;
            case 'message':
                /**
                 * When the user sends a message to your bot, your bot will receive the message as an Activity object of type message.
                 */
                response("Aceptado", 202);

                $conversation = ConversationController::getConversationData($channelId, $conversationId); // traigo el registro de la conversacion

                if (empty($conversation)) {
                    $conversation = ConversationController::createConversationData($channelId, $conversationId); // si no existe la conversacion, la creo
                }

                ConversationController::createConversationDetailData($conversation->id, $fromId, $fromName, $recipientId, $recipientName, $text);

                if (!$conversation->token && !$conversation->token_validado) { // entra si no hay token registrado, ni validacion
                    $reserva = ReservasController::getReservaByToken($text);

                    if ($reserva) {
                        ConversationController::setToken($conversation, $text);

                        $cardToSend[] = [ // el nombre de la variable debe de tener el [] al final para cumpli con la estructura de MSFT
                            "contentType" => "application/vnd.microsoft.card.hero",
                            "content" => array(
                                "text" => "Su Reserva en {$reserva->habitacion->hotel->nombre}\n\nSe ha encontrado una reserva con los siguientes datos:\n\n• Huésped: {$reserva->huesped->nombre_completo}\n\n• Habitación: {$reserva->habitacion->numero}\n\n• Fecha Entrada: {$reserva->fecha_inicio->format('d/m/Y')}\n\n• Fecha Salida: {$reserva->fecha_fin->format('d/m/Y')}\n\n¿Los datos son correctos?",
                                "buttons" => [
                                    [
                                        "type" => "postBack",
                                        "title" => "SI",
                                        "value" => "SI"
                                    ],
                                    [
                                        "type" => "postBack",
                                        "title" => "NO",
                                        "value" => "NO"
                                    ]
                                ]
                            )
                        ];
                        AnswerController::replyMessageWithAttachmentsToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, 'list', $cardToSend);
                    } else {
                        $messageToSend = "El código ingresado no existe, o no es válido, por favor ingresalo nuevamente.";
                        AnswerController::sendMessageToConversation($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $messageToSend);
                    }
                } else if ($conversation->token && !$conversation->token_validado) { // entra si hay token registrado pero no esta validado
                    switch (strtoupper($text)) {
                        case 'SI':
                            // el usuario confirma su reserva
                            ConversationController::setTokenValidation($conversation, true);
                            // se le envia el menu por primera vez
                            $reserva = ReservasController::getReservaByToken($conversation->token);
                            MenusController::sendMainMenu($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva);

                            $text = "Recuerda que en cualquier momento puedes escribir **MENU** para volver a verlo.";
                            AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $text);
                        break;
                        case 'NO':
                            // se limpia el token guardado y se invita al usuario a ingresar un nuevo token
                            ConversationController::setToken($conversation, null);
                            $messageToSend = "Por favor ingresa el código que te han dado en la recepción al momento del check-in para comenzar.";
                            AnswerController::sendMessageToConversation($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $messageToSend);
                        break;
                        default:
                            // se invita al usuario a decir que si o que no
                            $messageToSend = "Por favor confirme que los datos de la reserva son correctos, puede seleccionar una opción del resumen anterior, o escribir 'SI' o 'NO'.";
                            AnswerController::sendMessageToConversation($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $messageToSend);
                        break;
                    }
                } else { // hay token y esta validado
                    ConciergeController::understandClientMessage($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $conversation, $text);
                }
            break;
            default:
                response("Aceptado", 202);
                $messageToSend = "** INTERACCION NO RECONOCIDA [$type] **";
                AnswerController::sendMessageToConversation($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $messageToSend);
            break;
        }

        Log::info(" ");
        Log::info("***");
        Log::info(" ");
    }
}

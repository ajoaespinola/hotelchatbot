<?php

namespace App\Http\Controllers\BotFwrk;

use App\Http\Controllers\BotFwrk\Helpers\Cards;
use App\Http\Controllers\DespertadorController;
use App\Http\Controllers\LimpiezaHabitacionController;
use App\Http\Controllers\ReservaMesaController;
use App\Http\Controllers\ReservasController;
use App\Models\BotFwrk\Conversation;
use App\Models\Reserva\Reserva;
use App\Models\Restaurant\Restaurant;
use App\Models\Servicio\HorarioLimpiezaHabitacion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ConciergeController extends Controller
{
    public static function understandClientMessage($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, Conversation $conversation, $text){
        $text = Str::upper($text); // paso a mayusculas

        $reserva = ReservasController::getReservaByToken($conversation->token);

        $currentState = $conversation->propiedades['currentState'];
        Log::info("---> Estado actual: [$currentState]");

        // ConversationController::setProperties($conversation, 'currentState', $text);

        /*if (!isset($sessionData->data->currentState)) {
            ToolboxController::cleanStoredData($channelId, $conversationId, $fromId);
        }*/

        if (is_null($currentState)||($currentState == 1)) { // SIN VARIABLE DE ESTADO REGISTRADO O ESTADO ACTUAL 1: LIBRE
            switch ($text) {
                case 'HOLA':
                case 'BUENAS':
                case 'BUENOS DIAS':
                case 'BUENAS TARDES':
                case 'BUENAS NOCHES':
                    // SALUDOS
                    $response = "¡Hola, bienvenido {$reserva->huesped->nombre}!. Ingresa una petición para continuar, puedes por ejemplo preguntar el horario de apertura de la piscina. Recuerda que en cualquier momento puedes escribir **MENU** para ver todas las opciones.";
                    AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
                break;
                case '¿COMO ESTAS?':
                case 'COMO ESTAS?':
                case 'COMO ESTAS':
                    // COMO ESTAS
                    $response = "¡Me encuentro muy bien!, especialmente por ayudarte con tu estancia en el hotel. Ingresa una petición para continuar, puedes por ejemplo preguntar por la carta del restaurant en el servicio a cuartos. Recuerda que en cualquier momento puedes escribir **MENU** para ver todas las opciones.";
                    AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
                break;
                case '¡CHAU!':
                case 'CHAU!':
                case 'CHAU':
                case '¡ADIOS!':
                case 'ADIOS!':
                case 'ADIOS':
                    // CHAU
                    $response = "¡No nos dejes!, recuerda que estamos las 24 horas para ayudarte.";
                    AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
                break;
                case '¿QUE HACES?':
                case 'QUE HACES?':
                case 'QUE HACES':
                case '¿PARA QUE SERVIS?':
                case 'PARA QUE SERVIS?':
                case 'PARA QUE SERVIS':
                case '¿EN QUE ME AYUDAS?':
                case 'EN QUE ME AYUDAS?':
                case 'EN QUE ME AYUDAS':
                    // QUE HACES
                    $response = "Te ayudo a autogestionar tu estancia en **{$reserva->habitacion->hotel->nombre}**. Ingresa una petición para continuar, puedes por ejemplo preguntar por la carta del restaurant en el servicio a cuartos. Recuerda que en cualquier momento puedes escribir **MENU** para ver todas las opciones.";
                    AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
                break;
                case 'MENU':
                    // MENU PRINCIPAL
                    MenusController::sendMainMenu($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva);
                break;
                case 'CANCELAR':
                case 'CONFIRMAR':
                    // EL SISTEMA NO HACE NADA
                break;
                default:
                    // VA A RECONOCIMIENTO DE INTENCIONES
                    self::deeperUnderstanding($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $conversation, $reserva, $text);
                break;
            }
        } else if ($currentState == 2){ // ESTADO ACTUAL: OCUPADO EN ALGUN PROCESO
            if ($text == 'CANCELAR') {
                ConversationController::cleanProperties($conversation);
                $response = "Se ha cancelado el proceso actual. Ingresa una nueva petición para continuar...";
                AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
            } else {
                // VA A RECONOCIMIENTO DE INTENCIONES
                self::deeperUnderstanding($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $conversation, $reserva, $text);
            }
        }
    }

    private static function deeperUnderstanding($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, Conversation $conversation, Reserva $reserva, $text)
    {
        Log::info("<><><><><><><><><><><><><><><><><><><> DEEPER UNDERSTANDING");

        $currentState = $conversation->propiedades['currentState'];
        $luis = null;

        if ($currentState == 2) {
            Log::info("---> INTENCION DEFINIDA PREVIAMENTE, LA TRAIGO DE LAS PROPIEDADES DE LA CONVERSACION");
            $topScoringIntent = $conversation->propiedades['intent']['currentIntent'];
            Log::info("La intencion definida anteriormente es [". $topScoringIntent ."]");
        } else {
            Log::info("---> VA POR L.U.I.S");
            $luis = LuisController::askLuis($text);

            if ($luis){
                $topScoringIntent = $luis->topScoringIntent->intent;
                $topScoringIntentScore = $luis->topScoringIntent->score;

                // debo controlar el score para estar seguro de acertar la prediccion
                /*if ($topScoringIntentScore > 0.5){
                }*/

                Log::info("L.U.I.S. cree que es una intencion de tipo '". $topScoringIntent ."' con un score de " . $topScoringIntentScore);
            }
        }

        switch ($topScoringIntent) {
            // menus
            case "menu_ver_servicios":
                MenusController::sendServiciosMenu($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva);
            break;
            case "menu_ver_informaciones":
                MenusController::sendInformacionesMenu($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva);
            break;
            case "menu_ver_restaurantes":
                MenusController::sendRestaurantesMenu($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva);
            break;

            case "derivar_conversacion":
                $text = "Puedes ser derivado a un personal de atención al cliente seleccionando el botón de abajo.";
                $btnType = "openUrl";
                $btnValue = "DERIVAR";
                $btnUrl = "hotel/soporte/$conversationId";
                $cardToSend = Cards::prepareTextOneButtonHero(null, null, $text, $btnType, $btnValue, $btnUrl);
                return AnswerController::replyMessageWithAttachmentsToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, 'list', $cardToSend);
            break;

            // servicios
            case "servicio_despertador": // frase basica: CLAVE + HORA = 'despertarme a las 9 am'
                self::setWakeUpCall($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $conversation, $reserva, $luis, $text);
            break;
            case "servicio_limpieza": // frase basica: CLAVE = 'pueden venir a limpiar mi habitacion'
                self::askCleaningService($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $conversation, $reserva, $luis, $text);
            break;

            // informaciones
            case "info_direccion_hotel": // frase basica: CLAVE = 'cual es la direccion del hotel'
                self::showHotelAddress($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva);
            break;
            case "info_telefono_hotel": // frase basica: CLAVE = 'cual es el telefono del hotel'
                self::showHotelPhone($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva);
            break;
            case "info_telefono_habitacion": // frase basica: CLAVE = 'cual es el telefono de mi habitacion del hotel'
                self::showMyHotelPhone($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva);
            break;
            case "info_checkin_hotel": // frase basica: CLAVE = 'a partir de que hora se puede hacer checkin'
                self::showCheckInInfo($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva);
            break;
            case "info_checkout_hotel": // frase basica: CLAVE = 'hasta que hora se puede hacer el checkout'
                self::showCheckOutInfo($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva);
            break;
            case "info_mi_reserva": // frase basica: CLAVE = 'datos de mi reserva'
                self::showReservationInfo($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva);
            break;

            case "reservar_restaurant": // frase basica: CLAVE = 'reservar mesa para 2 en los leños mañana a las 15'
                self::bookTableAtRestaurant($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $conversation, $reserva, $luis, $text);
            break;

            default:
                $response = "Disculpa, no comprendo lo que me escribes, intenta escribir tu pedido de vuelta.";
                AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
            break;
        }

        Log::info("<><><><><><><><><><><><><><><><><><><> DEEPER UNDERSTANDING");
    }

    private static function askCleaningService($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, Conversation $conversation, Reserva $reserva, $luis, $text)
    {
        $currentState = $conversation->propiedades['currentState'];
        $currentIntent = 'servicio_limpieza';

        // CLAVE
        $claveFlag = false;
        $claveValue = "";
        // HORARIO
        $horarioFlag = false;
        $horarioValue = "";
        // DECISION
        $decisionFlag = false;
        $decisionValue = "";

        if ($currentState == 2) {
            do {
                /**
                 * al meter estas variables dentro de un while puedo hacer break en cualquier momento
                 * para no tener que validar por completo la peticion y parar hasta donde estoy procesando
                 */
                // CLAVE
                if (isset($conversation->propiedades['intent']['currentIntentData']['claveFlag'])) {
                    $claveFlag = true;
                    if (isset($conversation->propiedades['intent']['currentIntentData']['claveValue'])) {
                        $claveValue = $conversation->propiedades['intent']['currentIntentData']['claveValue'];
                        Log::info("CLAVE: ". $claveValue);
                    }
                }

                // HORARIO
                if (isset($conversation->propiedades['intent']['currentIntentData']['horarioFlag'])) {
                    $horarioFlag = true;
                    if (isset($conversation->propiedades['intent']['currentIntentData']['horarioValue'])) {
                        $horarioValue = $conversation->propiedades['intent']['currentIntentData']['horarioValue'];
                        Log::info("HORARIO: ". $horarioValue);
                    }
                } else {
                    $horarioFlag = true;
                    $horarioValue = $text;
                    Log::info("HORARIO A VALIDAR: ". $horarioValue);
                    break;
                }

                // DECISION
                if (isset($conversation->propiedades['intent']['currentIntentData']['decisionFlag'])) {
                    $decisionFlag = true;
                    if (isset($conversation->propiedades['intent']['currentIntentData']['decisionValue'])) {
                        $decisionValue = $conversation->propiedades['intent']['currentIntentData']['decisionValue'];
                        Log::info("DECISION: ". $decisionValue);
                    }
                } else {
                    $decisionFlag = true;
                    $decisionValue = $text;
                    Log::info("DECISION A VERIFICAR: ". $decisionValue);
                    break;
                }
            } while (0);
        } else {
            foreach ($luis->entities as $entity) {
                switch ($entity->type) {
                    case 'clave':
                        $claveFlag = true;
                        $claveValue = Str::upper($entity->entity);
                        Log::info("CLAVE: " . $claveValue);
                    break;
                }
            }
        }

        if (!$claveFlag){
            $response = "Disculpame, no puedo comprender la solicitud que me has hecho, ¿puedes cambiarla?.";
            return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
        }

        if (!$horarioFlag){
            Log::info("-- AUN NO COMPLETARON LOS PARAMETROS NECESARIOS PARA PEDIR LIMPIEZA DE HABITACION, OFREZCO HORARIOS");
            $currentIntentData = [
                "claveFlag" => $claveFlag,
                "claveValue" => $claveValue
            ];
            ConversationController::setCurrentState($conversation, 2); // seteo el estado de la conversacion como ocupado en un proceso
            ConversationController::setCurrentIntentData($conversation, $currentIntent, $currentIntentData);

            // traer horarios disponibles
            $availableTimeSlots = HorarioLimpiezaHabitacion::where('hotel_id', $reserva->habitacion->hotel->id)->get();

            $title = "HORARIOS DISPONIBLES";
            $text = "El servicio de limpieza de habitaciones del **{$reserva->habitacion->hotel->nombre}** es realizado dentro de alguno de los siguientes horarios.
            \n\nElija el rango que crea conveniente.\n\nEscribe 'CANCELAR' para anular el proceso.";
            $buttons = [];
            foreach ($availableTimeSlots as $ats){
                $formattedHoraInicio = Carbon::createFromTimeString($ats->hora_inicio)->format('H:i');
                $formattedHoraFin = Carbon::createFromTimeString($ats->hora_fin)->format('H:i');

                $button = [
                    "type" => "postBack",
                    "title" => "De {$formattedHoraInicio} a {$formattedHoraFin}",
                    "value" => "{$reserva->habitacion->hotel->id}_ats_{$ats->id}"
                ];
                array_push($buttons, $button);
            }
            $cardToSend = Cards::prepareHeroWithoutImgCard($title, null, $text, $buttons);
            return AnswerController::replyMessageWithAttachmentsToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, 'list', $cardToSend);
        } else {
            $selectedAts = explode("_", $horarioValue); // para prevenir que el user ingrese cualquier valor, se creo un patron unico hotelId_ats_horarioLimpiezaId

            if ($selectedAts[0] == $reserva->habitacion->hotel->id && $selectedAts[1] == 'ATS') {
                $horarioValue = $text;
            } else {
                $response = "Valor ingresado invalido, favor reintente eligiendo una de las opciones planteadas.";
                return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
            }
        }

        if (!$decisionFlag) {
            Log::info("-- AUN NO COMPLETO LOS PARAMETROS NECESARIOS PARA SOLICITAR LIMPIEZA, PIDO CONFIRMACION");
            $currentIntentData = [
                "claveFlag" => $claveFlag,
                "claveValue" => $claveValue,
                "horarioFlag" => $horarioFlag,
                "horarioValue" => $horarioValue
            ];
            ConversationController::setCurrentState($conversation, 2); // seteo el estado de la conversacion como ocupado en un proceso
            ConversationController::setCurrentIntentData($conversation, $currentIntent, $currentIntentData);

            // envio tarjeta de confirmacion
            $title = "SOLICITAR LIMPIEZA DE HABITACION";
            $subtitle = "Confirmación de datos";
            $selectedAts = HorarioLimpiezaHabitacion::where('id', $selectedAts[2])->first();
            $formattedHoraInicio = Carbon::createFromTimeString($selectedAts->hora_inicio)->format('H:i');
            $formattedHoraFin = Carbon::createFromTimeString($selectedAts->hora_fin)->format('H:i');
            $text = "Se solicitará la limpieza de su habitación dentro del rango horario **{$formattedHoraInicio}-{$formattedHoraFin}**. ¿Esta seguro?";
            $cardToSend = Cards::prepareBooleanConfirmationHeroCard($title, $subtitle, $text);
            return AnswerController::replyMessageWithAttachmentsToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, 'list', $cardToSend);
        } else {
            $decisionValida = ConversationController::validateConfirmDecision($decisionValue);

            if ($decisionValida == false){
                // DECISION INGRESADA NO VALIDA
                $response = "Valor ingresado invalido, favor reintente eligiendo una de las opciones planteadas.";
                return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
            } else {
                // DECISION VALIDA
                $servicioLimpieza = LimpiezaHabitacionController::bfCreate($reserva->id, $selectedAts[2]); // creo la solicitud de servicio

                if ($servicioLimpieza) {
                    ConversationController::cleanProperties($conversation); // limpio el storeBag ya que ya complete el proceso

                    $response = "Solicitud de limpieza de habitación realizada con éxito. Si lo deseas puedes realizar otras tareas o escribir **MENU** para ver todas las opciones.";
                    return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
                } else {
                    $response = "Ha ocurrido un error inesperado, favor reintentar.";
                    return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
                }
            }
        }
    }

    private static function setWakeUpCall($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, Conversation $conversation, Reserva $reserva, $luis, $text)
    {
        $currentState = $conversation->propiedades['currentState'];
        $currentIntent = 'servicio_despertador';

        // CLAVE
        $claveFlag = false;
        $claveValue = "";
        // HORA
        $horaFlag = false;
        $horaValue = "";
        // DECISION
        $decisionFlag = false;
        $decisionValue = "";

        if ($currentState == 2) {
            do {
                /**
                 * al meter estas variables dentro de un while puedo hacer break en cualquier momento
                 * para no tener que validar por completo la peticion y parar hasta donde estoy procesando
                 */
                // CLAVE
                if (isset($conversation->propiedades['intent']['currentIntentData']['claveFlag'])) {
                    $claveFlag = true;
                    if (isset($conversation->propiedades['intent']['currentIntentData']['claveValue'])) {
                        $claveValue = $conversation->propiedades['intent']['currentIntentData']['claveValue'];
                        Log::info("CLAVE: ". $claveValue);
                    }
                }

                // HORA
                if (isset($conversation->propiedades['intent']['currentIntentData']['horaFlag'])) {
                    $horaFlag = true;
                    if (isset($conversation->propiedades['intent']['currentIntentData']['horaValue'])) {
                        $horaValue = $conversation->propiedades['intent']['currentIntentData']['horaValue'];
                        Log::info("HORA: ". $horaValue);
                    }
                } else {
                    $horaFlag = true;
                    $horaValue = $text;
                    Log::info("HORA A VALIDAR VIA L.U.I.S.: ". $horaValue);

                    $text = "$claveValue $text";

                    ConversationController::cleanProperties($conversation); // limpio el storeBag porque reconstruyo la frase del usuario y la reeenvio a LUIS
                    return self::deeperUnderstanding($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $conversation, $reserva, $text);
                    break;
                }

                // DECISION
                if (isset($conversation->propiedades['intent']['currentIntentData']['decisionFlag'])) {
                    $decisionFlag = true;
                    if (isset($conversation->propiedades['intent']['currentIntentData']['decisionValue'])) {
                        $decisionValue = $conversation->propiedades['intent']['currentIntentData']['decisionValue'];
                        Log::info("DECISION: ". $decisionValue);
                    }
                } else {
                    $decisionFlag = true;
                    $decisionValue = $text;
                    Log::info("DECISION A VERIFICAR: ". $decisionValue);
                    break;
                }
            } while (0);
        } else {
            foreach ($luis->entities as $entity) {
                switch ($entity->type) {
                    case 'clave':
                        $claveFlag = true;
                        $claveValue = Str::upper($entity->entity);
                        Log::info("CLAVE: " . $claveValue);
                        break;
                    case 'builtin.datetimeV2.time':
                        $horaFlag = true;
                        $horaValue = $entity->resolution->values[0]->value;
                        Log::info("HORA A PROGRAMAR: " . $horaValue);
                        break;
                }
            }
        }

        if (!$claveFlag){
            $response = "Disculpame, no puedo comprender la solicitud que me has hecho, ¿puedes cambiarla?.";
            return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
        }

        if (!$horaFlag){
            Log::info("-- AUN NO COMPLETARON LOS PARAMETROS NECESARIOS PARA PROGRAMAR DESPERTADOR, PIDO HORA");
            $currentIntentData = [
                "claveFlag" => $claveFlag,
                "claveValue" => $claveValue
            ];
            ConversationController::setCurrentState($conversation, 2); // seteo el estado de la conversacion como ocupado en un proceso
            ConversationController::setCurrentIntentData($conversation, $currentIntent, $currentIntentData);

            $response = "Quieres programar un despertador. ¿A que hora quieres que te llamemos?. Escribe 'CANCELAR' para anular el proceso.";
            return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
        }

        if (!$decisionFlag) {
            Log::info("-- AUN NO COMPLETO LOS PARAMETROS NECESARIOS PARA PROGRAMAR DESPERTADOR, PIDO CONFIRMACION");
            $currentIntentData = [
                "claveFlag" => $claveFlag,
                "claveValue" => $claveValue,
                "horaFlag" => $horaFlag,
                "horaValue" => $horaValue
            ];
            ConversationController::setCurrentState($conversation, 2); // seteo el estado de la conversacion como ocupado en un proceso
            ConversationController::setCurrentIntentData($conversation, $currentIntent, $currentIntentData);

            // envio tarjeta de confirmacion
            $title = "PROGRAMAR DESPERTADOR";
            $subtitle = "Confirmación de datos";
            $formattedHora = Carbon::createFromTimeString($horaValue)->format('H:i');
            $text = "¿Desea crear un pedido de despertador a las {$formattedHora} horas?";
            $cardToSend = Cards::prepareBooleanConfirmationHeroCard($title, $subtitle, $text);
            return AnswerController::replyMessageWithAttachmentsToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, 'list', $cardToSend);
        } else {
            $decisionValida = ConversationController::validateConfirmDecision($decisionValue);

            if ($decisionValida == false){
                // DECISION INGRESADA NO VALIDA
                $response = "Valor ingresado invalido, favor reintente eligiendo una de las opciones planteadas.";
                return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
            } else {
                // DECISION VALIDA
                $despertador = DespertadorController::bfCreate($reserva->id, $horaValue); // creo el despertador

                if ($despertador) {
                    ConversationController::cleanProperties($conversation); // limpio el storeBag ya que ya complete el proceso

                    $response = "Programación de despertador realizada con éxito. Si lo deseas puedes realizar otras tareas o escribir **MENU** para ver todas las opciones.";
                    return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
                } else {
                    $response = "Ha ocurrido un error inesperado, favor reintentar.";
                    return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
                }
            }
        }
    }

    private static function showHotelAddress($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, Reserva $reserva)
    {
        $response = "El establecimiento **{$reserva->habitacion->hotel->nombre}** se encuentra ubicado en **{$reserva->habitacion->hotel->direccion}**";
        return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
    }

    private static function showHotelPhone($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, Reserva $reserva)
    {
        $response = "El teléfono del establecimiento **{$reserva->habitacion->hotel->nombre}** es **{$reserva->habitacion->hotel->telefono}**";
        return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
    }

    private static function showMyHotelPhone($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, Reserva $reserva)
    {
        $response = "Para que puedas recibir llamadas directamente en tu habitación te deben llamar al teléfono del establecimiento **{$reserva->habitacion->hotel->telefono}** y derivar a la extensión **{$reserva->habitacion->extension_tel}**";
        return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
    }

    private static function showCheckInInfo($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, Reserva $reserva)
    {
        $formattedHora = Carbon::createFromTimeString($reserva->habitacion->hotel->hora_checkin)->format('H:i');

        $response = "El check-in puede ser realizado desde las **{$formattedHora}** en el front desk del establecimiento.";
        return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
    }

    private static function showReservationInfo($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, Reserva $reserva)
    {
        $cardToSend[] = [ // el nombre de la variable debe de tener el [] al final para cumpli con la estructura de MSFT
            "contentType" => "application/vnd.microsoft.card.hero",
            "content" => array(
                "title" => "Su Reserva en {$reserva->habitacion->hotel->nombre}",
                "text" => "Se ha encontrado una reserva con los siguientes datos:\n\n• Huésped: {$reserva->huesped->nombre_completo}\n\n• Habitación: {$reserva->habitacion->numero}\n\n• Fecha Entrada: {$reserva->fecha_inicio->format('d/m/Y')}\n\n• Fecha Salida: {$reserva->fecha_fin->format('d/m/Y')}"
            )
        ];
        AnswerController::replyMessageWithAttachmentsToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, 'list', $cardToSend);
    }

    private static function showCheckOutInfo($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, Reserva $reserva)
    {
        $formattedHora = Carbon::createFromTimeString($reserva->habitacion->hotel->hora_checkout)->format('H:i');

        $response = "El check-out puede ser realizado sin costo hasta las **{$formattedHora}** en el front desk del establecimiento.";
        return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
    }

    private static function bookTableAtRestaurant($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, Conversation $conversation, Reserva $reserva, $luis, $text)
    {
        $currentState = $conversation->propiedades['currentState'];
        $currentIntent = 'reservar_restaurant';

        // CLAVE
        $claveFlag = false;
        $claveValue = "";
        // RESTAURANT
        $restaurantFlag = false;
        $restaurantValue = "";
        // COMENSALES
        $comensalesFlag = false;
        $comensalesValue = "";
        // FECHA
        $fechaFlag = false;
        $fechaValue = "";
        // DECISION
        $decisionFlag = false;
        $decisionValue = "";

        if ($currentState == 2) {
            do {
                /**
                 * al meter estas variables dentro de un while puedo hacer break en cualquier momento
                 * para no tener que validar por completo la peticion y parar hasta donde estoy procesando
                 */
                // CLAVE
                if (isset($conversation->propiedades['intent']['currentIntentData']['claveFlag'])) {
                    $claveFlag = true;
                    if (isset($conversation->propiedades['intent']['currentIntentData']['claveValue'])) {
                        $claveValue = $conversation->propiedades['intent']['currentIntentData']['claveValue'];
                        Log::info("CLAVE: ". $claveValue);
                    }
                }

                // RESTAURANT
                if (isset($conversation->propiedades['intent']['currentIntentData']['restaurantFlag'])) {
                    $restaurantFlag = true;
                    if (isset($conversation->propiedades['intent']['currentIntentData']['restaurantValue'])) {
                        $restaurantValue = $conversation->propiedades['intent']['currentIntentData']['restaurantValue'];
                        Log::info("RESTAURANT: ". $restaurantValue);
                    }
                }

                // FECHA HORA
                if (isset($conversation->propiedades['intent']['currentIntentData']['fechaFlag'])) {
                    $fechaFlag = true;
                    if (isset($conversation->propiedades['intent']['currentIntentData']['fechaValue'])) {
                        $fechaValue = $conversation->propiedades['intent']['currentIntentData']['fechaValue'];
                        Log::info("FECHA: ". $fechaValue);
                    }
                } else {
                    $fechaFlag = true;
                    $fechaValue = $text;
                    Log::info("FECHA A VALIDAR VIA L.U.I.S.: ". $fechaValue);

                    $text = "$claveValue $restaurantValue $text";

                    ConversationController::cleanProperties($conversation); // limpio el storeBag porque reconstruyo la frase del usuario y la reeenvio a LUIS
                    return self::deeperUnderstanding($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $conversation, $reserva, $text);
                    break;
                }

                // COMENSALES
                if (isset($conversation->propiedades['intent']['currentIntentData']['comensalesFlag'])) {
                    $comensalesFlag = true;
                    if (isset($conversation->propiedades['intent']['currentIntentData']['comensalesValue'])) {
                        $comensalesValue = $conversation->propiedades['intent']['currentIntentData']['comensalesValue'];
                        Log::info("CANTIDAD COMENSALES: ". $comensalesValue);
                    }
                } else {
                    $comensalesFlag = true;
                    $comensalesValue = $text;
                    Log::info("CANTIDA DE COMENSALES A VALIDAR VIA L.U.I.S.: ". $comensalesValue);

                    $text = "$claveValue $restaurantValue $fechaValue para $text personas";

                    ConversationController::cleanProperties($conversation); // limpio el storeBag porque reconstruyo la frase del usuario y la reeenvio a LUIS
                    return self::deeperUnderstanding($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $conversation, $reserva, $text);
                    break;
                }

                // DECISION
                if (isset($conversation->propiedades['intent']['currentIntentData']['decisionFlag'])) {
                    $decisionFlag = true;
                    if (isset($conversation->propiedades['intent']['currentIntentData']['decisionValue'])) {
                        $decisionValue = $conversation->propiedades['intent']['currentIntentData']['decisionValue'];
                        Log::info("DECISION: ". $decisionValue);
                    }
                } else {
                    $decisionFlag = true;
                    $decisionValue = $text;
                    Log::info("DECISION A VERIFICAR: ". $decisionValue);
                    break;
                }
            } while (0);
        } else {
            foreach ($luis->entities as $entity) {
                switch ($entity->type) {
                    case 'clave':
                        $claveFlag = true;
                        $claveValue = Str::upper($entity->entity);
                        Log::info("CLAVE: " . $claveValue);
                        break;
                    case 'restaurantes':
                        $restaurantFlag = true;
                        $restaurantValue = Str::upper($entity->resolution->values[0]);
                        Log::info("RESTAURANT: " . $restaurantValue);
                        break;
                    case 'builtin.datetimeV2.datetime':
                        if (isset($entity->role) && $entity->role == 'fecha_reserva') { // si encontro más de una fecha, solo me interesa la que tiene el rol correspondiente a este caso
                            $fechaFlag = true;
                            $fechaValue = $entity->resolution->values[0]->value;
                            Log::info("FECHA: " . $fechaValue);
                        }
                        break;
                    case 'builtin.number':
                        if (isset($entity->role) && $entity->role == 'cantidad_comensales') { // si encontro más de un numero, solo me interesa la que tiene el rol correspondiente a este caso
                            $comensalesFlag = true;
                            $comensalesValue = $entity->resolution->value;
                            Log::info("CANTIDAD COMENSALES: " . $comensalesValue);
                        }
                        break;
                }
            }
        }

        if (!$claveFlag){
            $response = "Disculpame, no puedo comprender la solicitud que me has hecho, ¿puedes cambiarla?.";
            return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
        }

        if (!$restaurantFlag){
            Log::info("-- AUN NO COMPLETARON LOS PARAMETROS NECESARIOS PARA RESERVAR MESA, OFREZCO RESTAURANTES");
            $currentIntentData = [
                "claveFlag" => $claveFlag,
                "claveValue" => $claveValue
            ];
            ConversationController::cleanProperties($conversation); // limpio el storeBag porque reconstruyo la frase del usuario y la reeenvio a LUIS
            return MenusController::sendRestaurantesMenu($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $reserva);
        }

        if (!$fechaFlag){
            Log::info("-- AUN NO COMPLETARON LOS PARAMETROS NECESARIOS PARA RESERVAR MESA, PIDO FECHA");
            $currentIntentData = [
                "claveFlag" => $claveFlag,
                "claveValue" => $claveValue,
                "restaurantFlag" => $restaurantFlag,
                "restaurantValue" => $restaurantValue
            ];
            ConversationController::setCurrentState($conversation, 2); // seteo el estado de la conversacion como ocupado en un proceso
            ConversationController::setCurrentIntentData($conversation, $currentIntent, $currentIntentData);

            $response = "¿En que fecha deseas hacer la reserva?. Escribe por ejemplo 'mañana a las 19' o 'hoy a las 3 de la tarde' o 'CANCELAR' para anular el proceso";
            return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
        }

        if (!$comensalesFlag){
            Log::info("-- AUN NO COMPLETARON LOS PARAMETROS NECESARIOS PARA RESERVAR MESA, PIDO CANTIDAD COMENSALES");
            $currentIntentData = [
                "claveFlag" => $claveFlag,
                "claveValue" => $claveValue,
                "restaurantFlag" => $restaurantFlag,
                "restaurantValue" => $restaurantValue,
                "fechaFlag" => $fechaFlag,
                "fechaValue" => $fechaValue
            ];
            ConversationController::setCurrentState($conversation, 2); // seteo el estado de la conversacion como ocupado en un proceso
            ConversationController::setCurrentIntentData($conversation, $currentIntent, $currentIntentData);

            $response = "¿Cuantas personas?. Escribe un número o el número en letras o 'CANCELAR' para anular el proceso";
            return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
        }

        if (!$decisionFlag) {
            Log::info("-- AUN NO COMPLETO LOS PARAMETROS NECESARIOS PARA RESERVAR MESA, PIDO CONFIRMACION");
            $currentIntentData = [
                "claveFlag" => $claveFlag,
                "claveValue" => $claveValue,
                "restaurantFlag" => $restaurantFlag,
                "restaurantValue" => $restaurantValue,
                "fechaFlag" => $fechaFlag,
                "fechaValue" => $fechaValue,
                "comensalesFlag" => $comensalesFlag,
                "comensalesValue" => $comensalesValue
            ];
            ConversationController::setCurrentState($conversation, 2); // seteo el estado de la conversacion como ocupado en un proceso
            ConversationController::setCurrentIntentData($conversation, $currentIntent, $currentIntentData);

            // envio tarjeta de confirmacion
            $title = "RESERVA DE MESA EN {$restaurantValue}";
            $subtitle = "Confirmación de datos";
            $formattedFecha = Carbon::createFromFormat('Y-m-d H:i:s', $fechaValue)->format('d/m/Y H:i');
            $text = "¿Desea reservar una mesa para {$comensalesValue} personas ingresando el {$formattedFecha}?";
            $cardToSend = Cards::prepareBooleanConfirmationHeroCard($title, $subtitle, $text);
            return AnswerController::replyMessageWithAttachmentsToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, 'list', $cardToSend);
        } else {
            $decisionValida = ConversationController::validateConfirmDecision($decisionValue);

            if ($decisionValida == false){
                // DECISION INGRESADA NO VALIDA
                $response = "Valor ingresado invalido, favor reintente eligiendo una de las opciones planteadas.";
                return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
            } else {
                // DECISION VALIDA
                $restaurante = Restaurant::whereRaw('LOWER(`nombre`) like ?', ["%". strtolower($restaurantValue) . "%"])->first();

                $reservaMesa = ReservaMesaController::bfCreate($reserva->id, $restaurante->id, $comensalesValue, $fechaValue); // creo la reserva

                if ($reservaMesa) {
                    ConversationController::cleanProperties($conversation); // limpio el storeBag ya que ya complete el proceso

                    $response = "Reserva de mesa realizada con éxito. Si lo deseas puedes realizar otras tareas o escribir **MENU** para ver todas las opciones.";
                    return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
                } else {
                    $response = "Ha ocurrido un error inesperado, favor reintentar.";
                    return AnswerController::replyMessageToId($serviceUrl, $fromId, $fromName, $recipientId, $recipientName, $conversationId, $activityId, $response);
                }
            }
        }
    }
}

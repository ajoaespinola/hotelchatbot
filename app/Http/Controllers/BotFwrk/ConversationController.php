<?php

namespace App\Http\Controllers\BotFwrk;

use App\Models\BotFwrk\Conversation;
use App\Models\BotFwrk\ConversationDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ConversationController extends Controller
{
    public static function createConversationData($channelId, $conversationId) {
        try {
            DB::beginTransaction();

            $conversacion = new Conversation();

            $conversacion->canal_id = $channelId;
            $conversacion->conversacion_id = $conversationId;

            $conversacion->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            return $conversacion;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(">> CHAT LOGGER -- ERROR: " . $e->getMessage());

            return null;
        }
    }

    public static function createConversationDetailData($bot_conversacion_id, $emisor_id, $emisor_nombre, $receptor_id, $receptor_nombre, $texto) {
        try {
            DB::beginTransaction();

            $conversacionDetalle = new ConversationDetail();

            $conversacionDetalle->bot_conversacion_id = $bot_conversacion_id;
            $conversacionDetalle->emisor_id = $emisor_id;
            if ($emisor_nombre) {
                $conversacionDetalle->emisor_nombre = $emisor_nombre;
            }
            $conversacionDetalle->receptor_id = $receptor_id;
            if ($receptor_nombre) {
                $conversacionDetalle->emisor_nombre = $emisor_nombre;
            }
            $conversacionDetalle->receptor_nombre = $receptor_nombre;
            $conversacionDetalle->texto = $texto;

            $conversacionDetalle->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            return $conversacionDetalle;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(">> CHAT LOGGER -- ERROR: " . $e->getMessage());

            return null;
        }
    }

    public static function getConversationData($channelId, $conversationId) {
        return Conversation::where('canal_id', $channelId)->where('conversacion_id', $conversationId)->first();
    }

    public static function setToken(Conversation $conversation, $token) {
        try {
            DB::beginTransaction();

            $conversation->token = $token;

            $conversation->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(">> CHAT LOGGER -- ERROR: " . $e->getMessage());
            return false;
        }
    }

    public static function setTokenValidation(Conversation $conversation, $tokenValidation = false) {
        try {
            DB::beginTransaction();

            $conversation->token_validado = $tokenValidation;

            $conversation->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(">> CHAT LOGGER -- ERROR: " . $e->getMessage());
            return false;
        }
    }

    public static function cleanProperties(Conversation $conversation) {
        try {
            DB::beginTransaction();

            $conversation->propiedades = null;

            $conversation->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(">> CHAT LOGGER -- ERROR: " . $e->getMessage());
            return false;
        }
    }

    public static function setProperties(Conversation $conversation, $index, $value) {
        try {
            DB::beginTransaction();

            $conversationProperties = $conversation->propiedades;

            $conversationProperties[$index] = $value;

            $conversation->propiedades = $conversationProperties;

            $conversation->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(">> CHAT LOGGER -- ERROR: " . $e->getMessage());
            return false;
        }
    }

    public static function setCurrentState(Conversation $conversation, $currentState) {
        self::setProperties($conversation, 'currentState', $currentState);
    }

    public static function setCurrentIntentData(Conversation $conversation, $currentIntent, $currentIntentData) {
        $intent = [
            'currentIntent' => $currentIntent,
            'currentIntentData' => $currentIntentData,
        ];
        self::setProperties($conversation, 'intent', $intent);
    }

    public static function validateConfirmDecision($decision) {
        if ($decision == "CONFIRMAR"){
            return true;
        } else {
            return false;
        }
    }

    public function showChatLog(Request $request)
    {
        $this->validate($request, [
            'botpin' => 'required',
        ]);

        $chat = ConversationDetail::whereHas('conversation', function ($query) use ($request) {
            $query->where('token', $request->botpin);
        })->orderBy('created_at', 'ASC')->get();

        if (count($chat) > 0) {
            return view('support.logviewer')->with('chat', $chat);
        } else {
            \Session::flash("error", "El pin ingresado no es valido o no existe.");
            return back()->withInput();
        }
    }
}

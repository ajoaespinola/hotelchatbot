<?php

namespace App\Http\Controllers\BotFwrk;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class AuthorizationController extends Controller
{
    /**
     * To communicate with the Bot Connector service, you must specify an access token in the Authorization header of each API request...
     */
    public static function getAccessToken()
    {
        Log::info(">> getAccessToken -> BOT FWORK: Obteniendo access token... ");

        $http_client = new Client();
        try {
            $http_client_response = $http_client->request('POST', 'https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token', [
                'headers' => [
                    'Host' => 'login.microsoftonline.com',
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => '21a93b6b-6415-4f0a-844e-d343d5832974',
                    'client_secret' => '8:xpP5_GojtiVI?LGSAsap9GYn3@fR[V', // UNIR
                    'scope' => 'https://api.botframework.com/.default'
                ],
            ]);

            $msft_response = $http_client_response->getBody()->getContents();

            // Log::debug(">> getAccessToken <- BOT FWORK: Respuesta: " . $msft_response);

            $msft_response = json_decode($msft_response);

            $token_type = $msft_response->token_type;
            $expires_in = $msft_response->expires_in;
            $ext_expires_in = $msft_response->ext_expires_in;
            $access_token = $msft_response->access_token;

            return $token_type . " " . $access_token;
        } catch (RequestException $e) {
            Log::error(Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                Log::error(Psr7\str($e->getResponse()));
            }
        }
    }
}

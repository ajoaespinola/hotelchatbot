<?php

namespace App\Http\Controllers;

use App\Models\Restaurant\ReservaMesa;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReservaMesaController extends Controller
{
    public static function bfCreate($reserva_id, $restaurante_id, $cantidad_personas, $fecha_reserva)
    {
        try {
            DB::beginTransaction();

            $reservaMesa = new ReservaMesa();

            $reservaMesa->reserva_id = $reserva_id;
            $reservaMesa->restaurante_id = $restaurante_id;

            $reservaMesa->cantidad_personas = $cantidad_personas;

            $formattedFecha = Carbon::createFromFormat('Y-m-d H:i:s', $fecha_reserva);

            $reservaMesa->fecha_reserva = $formattedFecha->format('Y/m/d');
            $reservaMesa->hora_llegada = $formattedFecha->format('H:i:s');

            $reservaMesa->estado_res_mesa_id = 1; // activo por defecto

            $reservaMesa->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            return $reservaMesa;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(">> ReservaMesaController -- ERROR: " . $e->getMessage());
            return false;
        }
    }
}

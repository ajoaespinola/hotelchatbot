<?php

namespace App\Http\Controllers;

use App\Models\Hotel\Hotel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $hoteles = Hotel::all();

        return view('hoteles.index', compact('hoteles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('hoteles.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|string',
            'direccion' => 'required|string',
            'telefono' => 'required|string',
            'extension_recepcion' => 'required|integer',
            'hora_checkin' => 'nullable|date_format:H:i',
            'hora_checkout' => 'nullable|date_format:H:i',
        ]);

        try {
            DB::beginTransaction();

            $hotel = new Hotel();

            $hotel->nombre = $request->nombre;
            $hotel->direccion = $request->direccion;
            $hotel->telefono = $request->telefono;
            $hotel->extension_recepcion = $request->extension_recepcion;
            if ($request->filled('hora_checkin')) {
                $hotel->hora_checkin = $request->hora_checkin;
            }
            if ($request->filled('hora_checkout')) {
                $hotel->hora_checkout = $request->hora_checkout;
            }

            $hotel->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            \Session::flash("success", "El hotel fue creado exitosamente.");
            return redirect()->route('hoteles.show', [$hotel]);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Hotel $hotel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show(Hotel $hotel)
    {
        return view('hoteles.mostrar', compact('hotel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Hotel $hotel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(Hotel $hotel)
    {
        return view('hoteles.editar', compact('hotel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Hotel $hotel
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function update(Request $request, Hotel $hotel)
    {
        $this->validate($request, [
            'nombre' => 'required|string',
            'direccion' => 'required|string',
            'telefono' => 'required|string',
            'extension_recepcion' => 'required|integer',
            'hora_checkin' => 'nullable|date_format:H:i',
            'hora_checkout' => 'nullable|date_format:H:i',
        ]);

        try {
            DB::beginTransaction();

            $hotel->nombre = $request->nombre;
            $hotel->direccion = $request->direccion;
            $hotel->telefono = $request->telefono;
            $hotel->extension_recepcion = $request->extension_recepcion;
            if ($request->filled('hora_checkin')) {
                $hotel->hora_checkin = $request->hora_checkin;
            } else {
                $hotel->hora_checkin = null;
            }
            if ($request->filled('hora_checkout')) {
                $hotel->hora_checkout = $request->hora_checkout;
            } else {
                $hotel->hora_checkout = null;
            }

            $hotel->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            \Session::flash("success", "El hotel fue editado exitosamente.");
            return redirect()->route('hoteles.show', [$hotel]);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Hotel $hotel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        try {
            DB::beginTransaction();

            $hotel->delete();

            DB::commit();

            \Session::flash("success", "El hotel fue borrado exitosamente.");
            return redirect()->route('hoteles.index');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function changeWorkingHotel(Request $request) {
        return session(['selectedHotelId' => $request->selectedHotelId]);
    }
}

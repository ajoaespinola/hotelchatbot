<?php

namespace App\Http\Controllers;

use App\Models\Servicio\Despertador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DespertadorController extends Controller
{
    public static function bfCreate($reserva_id, $hora_despertador)
    {
        try {
            DB::beginTransaction();

            $despertador = new Despertador();

            $despertador->reserva_id = $reserva_id;
            $despertador->hora_despertador = $hora_despertador;
            $despertador->estado_despertador_id = 1; // pendiente por defecto

            $despertador->save(); // The created_at and updated_at timestamps will automatically be set when the save method is called

            DB::commit();

            return $despertador;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(">> DespertadorController -- ERROR: " . $e->getMessage());
            return false;
        }
    }
}

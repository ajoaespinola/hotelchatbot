<?php

namespace App\Http\ViewComposers;

use App\Models\Hotel\Hotel;
use Illuminate\View\View;

class HotelSelectorList
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $hoteles = Hotel::getSelectorList()->get();

        $view->with('hoteles', $hoteles);
    }
}
<?php

namespace App\Http\ViewComposers;

use App\Models\Hotel\Hotel;
use Illuminate\View\View;

class GetSelectedHotelData
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if (session('selectedHotelId')){
            $selectedHotel = Hotel::where('id', session('selectedHotelId'))->first();

            $view->with('selectedHotel', $selectedHotel);
        }
    }
}
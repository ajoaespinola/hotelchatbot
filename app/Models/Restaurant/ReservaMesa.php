<?php

namespace App\Models\Restaurant;

use Illuminate\Database\Eloquent\Model;

class ReservaMesa extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'res_mesas';

    public function reserva()
    {
        return $this->belongsTo('App\Models\Reserva\Reserva', 'reserva_id');
    }

    public function estado()
    {
        return $this->belongsTo('App\Models\Restaurant\EstadoReservaMesa', 'estado_res_mesa_id');
    }
}

<?php

namespace App\Models\Restaurant;

use Illuminate\Database\Eloquent\Model;

class EstadoReservaMesa extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'estado_res_mesas';
}

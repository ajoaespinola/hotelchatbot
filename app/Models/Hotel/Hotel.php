<?php

namespace App\Models\Hotel;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hoteles';

    public function scopeGetSelectorList($query)
    {
        return $query;
    }

}

<?php

namespace App\Models\Hotel;

use Illuminate\Database\Eloquent\Model;

class Habitacion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'habitaciones';

    public function hotel()
    {
        return $this->belongsTo('App\Models\Hotel\Hotel', 'hotel_id');
    }

    public function estado()
    {
        return $this->belongsTo('App\Models\Hotel\EstadoHabitacion', 'estado_habitacion_id');
    }
}

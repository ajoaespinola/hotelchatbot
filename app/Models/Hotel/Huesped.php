<?php

namespace App\Models\Hotel;

use Illuminate\Database\Eloquent\Model;

class Huesped extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'huespedes';

    public function hotel()
    {
        return $this->belongsTo('App\Models\Hotel\Hotel', 'hotel_id');
    }

    public function getNombreCompletoAttribute()
    {
        return "{$this->nombre} {$this->apellido}";
    }
}

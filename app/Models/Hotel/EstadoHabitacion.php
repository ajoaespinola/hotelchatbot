<?php

namespace App\Models\Hotel;

use Illuminate\Database\Eloquent\Model;

class EstadoHabitacion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'estado_habitaciones';
}

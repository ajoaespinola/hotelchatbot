<?php

namespace App\Models\Reserva;

use Illuminate\Database\Eloquent\Model;

class EstadoReserva extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'estado_reservas';
}

<?php

namespace App\Models\Reserva;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reservas';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'fecha_inicio',
        'fecha_fin',
        'created_at',
        'updated_at'
    ];

    public function habitacion()
    {
        return $this->belongsTo('App\Models\Hotel\Habitacion', 'habitacion_id');
    }

    public function huesped()
    {
        return $this->belongsTo('App\Models\Hotel\Huesped', 'huesped_id');
    }

    public function estado()
    {
        return $this->belongsTo('App\Models\Reserva\EstadoReserva', 'estado_reserva_id');
    }
}

<?php

namespace App\Models\BotFwrk;

use Illuminate\Database\Eloquent\Model;

class ConversationDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bot_conversacion_detalles';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function conversation()
    {
        return $this->belongsTo('App\Models\BotFwrk\Conversation', 'bot_conversacion_id');
    }
}

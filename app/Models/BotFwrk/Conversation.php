<?php

namespace App\Models\BotFwrk;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bot_conversaciones';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types (propiedades column is JSON, so we cast to array).
     *
     * @var array
     */
    protected $casts = [
        'propiedades' => 'array',
    ];
}

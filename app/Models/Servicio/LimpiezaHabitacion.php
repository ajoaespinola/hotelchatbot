<?php

namespace App\Models\Servicio;

use Illuminate\Database\Eloquent\Model;

class LimpiezaHabitacion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'limpieza_habs';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function reserva()
    {
        return $this->belongsTo('App\Models\Reserva\Reserva', 'reserva_id');
    }

    public function estado()
    {
        return $this->belongsTo('App\Models\Servicio\EstadoLimpiezaHabitacion', 'estado_limpieza_hab_id');
    }

    public function horario()
    {
        return $this->belongsTo('App\Models\Servicio\HorarioLimpiezaHabitacion', 'horario_limpieza_hab_id');
    }
}

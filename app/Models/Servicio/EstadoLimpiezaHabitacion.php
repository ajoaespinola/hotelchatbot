<?php

namespace App\Models\Servicio;

use Illuminate\Database\Eloquent\Model;

class EstadoLimpiezaHabitacion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'estado_limpieza_habs';
}

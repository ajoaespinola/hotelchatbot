<?php

namespace App\Models\Servicio;

use Illuminate\Database\Eloquent\Model;

class EstadoDespertador extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'estado_despertadores';
}

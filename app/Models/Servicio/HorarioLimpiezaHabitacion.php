<?php

namespace App\Models\Servicio;

use Illuminate\Database\Eloquent\Model;

class HorarioLimpiezaHabitacion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'horario_limpieza_habs';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
}

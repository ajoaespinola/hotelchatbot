<?php

namespace App\Models\Servicio;

use Illuminate\Database\Eloquent\Model;

class Despertador extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'despertadores';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function reserva()
    {
        return $this->belongsTo('App\Models\Reserva\Reserva', 'reserva_id');
    }

    public function estado()
    {
        return $this->belongsTo('App\Models\Servicio\EstadoDespertador', 'estado_despertador_id');
    }
}
